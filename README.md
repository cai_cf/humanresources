这是一个OA 项目，基于ssh编写，使用 mysql 数据库，适合初学者代码比较简单，这个项目是我当时的毕业项目，现在给贡献出来，大家在一起可以多多交流些技术，互相提高。
主要完成的模块有 部门管理、职位管理、用户管理、档案管理、薪酬管理、人员调动、人员培训、员工激励、招聘管理、考勤管理
还有一些没有完成的，项目完成时间仓促有不足的地方可以互相交流

初始用户名 ： 3 密码 ：123123 项目访问路径 ： [http://localhost:8080/humanresources/](http://)

登陆页：

![输入图片说明](http://git.oschina.net/uploads/images/2015/1223/221456_7dd6ed1b_376915.png "在这里输入图片标题")

登录成功进入首页

![输入图片说明](http://git.oschina.net/uploads/images/2015/1223/222112_ae616da7_376915.png "在这里输入图片标题")

  ^_^ 导航功能没有做
 点击部门管理可以进行部门添加

![输入图片说明](http://git.oschina.net/uploads/images/2015/1223/222252_8a00c558_376915.png "在这里输入图片标题")

点击用户管理，可以进行用户添加

![输入图片说明](http://git.oschina.net/uploads/images/2015/1223/222349_ae2d451c_376915.png "在这里输入图片标题")

档案管理-档案登记

![输入图片说明](http://git.oschina.net/uploads/images/2015/1223/222450_3f4b99ec_376915.png "在这里输入图片标题")

职位发布

![输入图片说明](http://git.oschina.net/uploads/images/2015/1223/222554_9ef459b1_376915.png "在这里输入图片标题")

简历登记

![输入图片说明](http://git.oschina.net/uploads/images/2015/1223/223227_53b30354_376915.png "在这里输入图片标题")

培训记录

![输入图片说明](http://git.oschina.net/uploads/images/2015/1223/223314_463c4313_376915.png "在这里输入图片标题")

谢谢大家支持

QQ 群 ： 335102947
