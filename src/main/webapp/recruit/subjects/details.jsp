<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!-- 试题详细页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>
		
		<!-- 导入table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css">
		
	</head>
	<body>
	<table width="100%">
			<tr>
				<td>
					<font color="#0000CC">您正在做的业务是：人力资源--招聘管理--招聘考试题库管理--试题查询 </font>
				</td>
			</tr>
			<tr>
				<td align="right">
					<input type="button" class="BUTTON_STYLE1" value="返回" onClick="history.back();">
				</td>
			</tr>
		</table>
		<table class="TABLE_STYLE1" border="1" width="100%" cellpadding=0>
			<tr class="TR_STYLE1">
			  <td width="10%" class="TD_STYLE1"> 登记时间 </td>
				<td width="14%"  class="TD_STYLE2">&nbsp;</td>
			<td width="14%"  class="TD_STYLE2">&nbsp;</td>
			<td width="14%"  class="TD_STYLE2">&nbsp;</td>
			<td width="14%"  class="TD_STYLE2">&nbsp;</td>
			</tr>
			<tr>
					<td class="TD_STYLE1">
						题干
					</td>
					<td class="TD_STYLE2" colspan="7" height="68">
						<s:property value="#request.getQuestionsById.hrQueContent"/>
					</td>
			</tr>
			<tr>
					<td class="TD_STYLE1">
						答案a
					</td>
					<td class="TD_STYLE2" colspan="7">
							<s:property value="#request.getQuestionsById.hrQueKeyA"/>
					</td>
				</tr>
			<tr>
					<td class="TD_STYLE1">
						答案b
					</td>
					<td class="TD_STYLE2" colspan="7">
						<s:property value="#request.getQuestionsById.hrQueKeyB"/>
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						答案c
					</td>
					<td class="TD_STYLE2" colspan="7">
					<s:property value="#request.getQuestionsById.hrQueKeyC"/>
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						答案d
					</td>
					<td class="TD_STYLE2" colspan="7">
						<s:property value="#request.getQuestionsById.hrQueKeyD"/>
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						正确答案
					</td>
					<td class="TD_STYLE2">
						<s:property value="#request.getQuestionsById.hrQueKeyCorrect"/>
					</td>
					<td class="TD_STYLE2">&nbsp;</td>
					<td class="TD_STYLE2">&nbsp;</td>
					<td class="TD_STYLE2">
				
				</tr>
		</table>
	</body>
</html>
