<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 此页面为职位信息的修改和删除操作 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'a.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
		
		<!-- 导入table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css">
		<!-- 定义删除方法 -->
		<script type="text/javascript">
			function delet(id){
				var flag=confirm("确定要删除嘛??");
				if(flag){
					window.location.href="/humanresources/recruit/deleMajorPut.action?id="+id;
					return true;
				}
					return false;
				
			}
		</script>
  </head>
  

	<body>
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--招聘管理--职位发布管理--职位发布变更
						</font>
					</td>
				</tr>
				<tr>
				<!-- 引用职位添加页面 -->
					<td align="right">
						<input type="button" value="添加" class="BUTTON_STYLE1"
							onclick="window.location.href='/humanresources/recruit/release/details.jsp'" />
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr>
					<td width="15%" class="TD_STYLE1">职位名称</td>
					<td width="15%" class="TD_STYLE1">招聘类型</td>
					<td width="10%" class="TD_STYLE1">招聘人数</td>
					<td width="15%" class="TD_STYLE1">发布时间</td>
					<td width="15%" class="TD_STYLE1">截至时间</td>
					<td width="15%" class="TD_STYLE1">修改</td>
					<td width="15%" class="TD_STYLE1">删除</td>
				</tr>
				<!-- 此处为职位表信息的查询 -->
					<s:iterator value="#request.selectMajorPut">
						<tr>
							<td class="TD_STYLE2">
								<s:property value="hrPutName"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrPutType"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrPutNumber"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrPutBeginDate"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrPutEndDate"/>
							</td>
							
							<!-- 职位修改查询 -->
							<td class="TD_STYLE2">
								<a href="/humanresources/recruit/modify_changingMajorPutSelect.action?id=${hrPutId} ">修改</a>
							</td>
							<td class="TD_STYLE2">
							
							<!-- 职位删除 -->
								<a style="text-decoration: underline; cursor: pointer;" onclick="delet('${hrPutId}')">删除</a>
							</td>
						</tr>
					</s:iterator>
			</table>
			<p>&nbsp;&nbsp;总数：1例 &nbsp;&nbsp;&nbsp;当前第 1 页  &nbsp;&nbsp;&nbsp;共 1 页  &nbsp;&nbsp;&nbsp;跳到第 <input name=page type=text class=input1 size=1> 页&nbsp;&nbsp;<input type=image src="../../../images/go.bmp" width=18 height=18 border=0>
	</body>
</html>