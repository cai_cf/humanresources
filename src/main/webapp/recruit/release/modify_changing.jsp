<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 职位发布修改页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<!-- 导入table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css" />
		<!-- 导入日期控件.js -->
		<script language="javascript" type="text/javascript" src="/humanresources/My97DatePicker/WdatePicker.js"></script>
		<!-- 导入jquery.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery-1.8.0.js"></script>
		<!-- 导入validator.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery.validate.js"></script>
		<!-- 导入metadate.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery.metadata.js"></script>
		<!-- 导入messages_cn.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery.validate.messages_cn.js" charset="utf-8"></script>
		<!-- 导入modify_changing.js -->
		<script type="text/javascript" src="/humanresources/javascript/recruit/release/modify_changing.js"></script>
	</head>

	<body onLoad="check()">
	<!-- 提交form表单提交到修改职位action -->
		<form action="/humanresources/recruit/updateMajorPut.action" id="ump" method="post">
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--招聘管理--职位发布管理--职位发布修改--详细 </font>
					</td>
				</tr>
				<tr>
					<td align="right">
					<!-- 进行职位信息修改 -->
						<input type="submit" value="重新申请" class="BUTTON_STYLE1">
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">

				<tr class="TD_STYLE2">
					<td class="TD_STYLE1">
						职位分类
					</td>
					<td width="15%" class="TD_STYLE2">
					<!-- 因为需要根据此id修改所以隐藏 -->
					    <input type="hidden" name="majorPut.hrPutId" value="${majorPut.hrPutId}">
						<input type="text" value="${majorPut.hrPutName}" name="majorPut.hrPutName"  class="INPUT_STYLE2 required"/>
					</td>
					<td width="10%" class="TD_STYLE1">
				  招聘人数
					</td>
					<td width="15%" class="TD_STYLE2">
						<input type="text" value="${majorPut.hrPutNumber}" name="majorPut.hrPutNumber" class="INPUT_STYLE2 required"/>
					</td>
					<td width="10%" class="TD_STYLE1">招聘类型</td>
					<td width="15%" class="TD_STYLE2">
					<input type="text" value="${majorPut.hrPutType}" name="majorPut.hrPutType" class="INPUT_STYLE2 required"/>
				    	
				    </td>
					 
				</tr>
			
				<tr class="TD_STYLE2">
					<td width="8%" class="TD_STYLE1">
						发布日期
					</td>
					<td width="15%" class="TD_STYLE2">
						<input type="text" class="INPUT_STYLE2 Wdate required" value="${majorPut.hrPutBeginDate}" name="majorPut.hrPutBeginDate" onClick="WdatePicker()"   readonly="readonly">
					</td>
					<td class="TD_STYLE1">截止时间 </td>
					<td class="TD_STYLE2">
						<input type="text" value="${majorPut.hrPutEndDate}" name="majorPut.hrPutEndDate" onClick="WdatePicker()"   class="INPUT_STYLE2 Wdate required"/>
					</td>
				</tr>

				<tr class="TD_STYLE2">
					<td width="10%" class="TD_STYLE1">
						招聘要求
					</td>
					<td class="TD_STYLE2" colspan="7">
						<textarea  name="majorPut.hrPutRequire" rows="5" cols="160"  class="TEXTAREA_STYLE1 required" ><s:property value="#request.majorPut.hrPutRequire"/></textarea>
					</td>
				</tr>

				<tr class="TD_STYLE2">
					<td width="10%" class="TD_STYLE1">
						职位状态
					</td>
					<td class="TD_STYLE2" colspan="7">
						<textarea name="majorPut.hrPutStatus" rows="5" cols="160"  class="TEXTAREA_STYLE1 required"><s:property value="#request.majorPut.hrPutStatus"/></textarea>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>