<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!-- 此页面为职位发布登记页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>
		<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
		
		<!-- 导如表格样式css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css" />
		<!-- 导入日期控件js -->
		<script language="javascript" type="text/javascript" src="/humanresources/My97DatePicker/WdatePicker.js"></script>
		<!-- 导入定义的职位发布css -->
		<link rel="stylesheet" href="/humanresources/css/recruit.css" type="text/css" />
		<!-- 导入jquery.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery-1.8.0.js"></script>
		<!-- 导入定义的职位发布 js-->
		<script type="text/javascript" src="/humanresources/javascript/recruit/release/details.js" charset="gbk"/></script>
		<!-- 导入validator.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery.validate.js"></script>
		<!-- 导入metadate.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery.metadata.js"></script>
		<!-- 导入messages_cn.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery.validate.messages_cn.js" charset="utf-8"></script>

	</head>

	<body>
	
		<!-- 此form跳转的action为职位发布登记添加 -->
		<form id="commentForm" action="/humanresources/recruit/addMajorPutRecruit.action" method="post" onSubmit="return check()">
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是：人力资源--招聘管理--职位发布管理--职位发布查询--详细 </font>
					</td>
				</tr>
				<tr>
					<td align="right">
						<input type="submit" value="确认申请" class="BUTTON_STYLE1" id="submit">
						<input type="button" value="返回" class="BUTTON_STYLE1">
					</td>
				</tr>
			</table>
			<table width="10%" border="1" cellpadding=0 cellspacing=1 class="TABLE_STYLE1">

				<tr class="TD_STYLE2">
					<td class="TD_STYLE1">
						职位分类
					</td>
					<td width="15%" class="TD_STYLE2">
						<input type="text" id="majName" name="majorPut.hrPutName" onblur="kind()"  class="INPUT_STYLE2 required" minlength="2"/>
					</td>
					<td width="15%"  class="TD_STYLE2" id="maj"></td>
					
					<td width="10%" class="TD_STYLE1">招聘类型</td>
					<td width="15%" class="TD_STYLE2">
						<select name="majorPut.hrPutType" id="mt" class="INPUT_STYLE2 required">
					  			<option value="请选择"> --请选择--</option>
								<option value="校园招聘">校园招聘</option>
								<option value="社会招聘">社会招聘</option>
				    	</select>
				    </td>
				    <td colspan="3"  class="TD_STYLE2" id="emt"></td>
				</tr>
				<tr class="TD_STYLE2">
					<td width="8%" class="TD_STYLE1">
						发布日期
					</td>
					<td width="15%" class="TD_STYLE2">
						<input type="text" class="Wdate" name="majorPut.hrPutBeginDate" onClick="WdatePicker()" id="INPUT_STYLE2"  readonly="readonly">
					</td>
					<td width="15%" class="TD_STYLE2"></td>
					<td class="TD_STYLE1">截止时间 </td>
					<td class="TD_STYLE2"><input type="text" name="majorPut.hrPutEndDate" onClick="WdatePicker()"  class="Wdate" readonly="readonly" id="INPUT_STYLE2"/></td>
					<td width="15%" class="TD_STYLE2"></td>
				</tr>
				<tr  class="TD_STYLE2">
						
					<td width="10%" class="TD_STYLE1">
				  招聘人数
					</td>
					<td width="15%" class="TD_STYLE2">
						<input type="text" name="majorPut.hrPutNumber" class="INPUT_STYLE2 required digits"/>
					</td>
					<td colspan="4"  class="TD_STYLE2"></td>
				</tr>
				

				<tr  class="TD_STYLE2">
					<td width="10%" class="TD_STYLE1">
						招聘要求
					</td>
					<td class="TD_STYLE2" colspan="7">
						<textarea name="majorPut.hrPutRequire" class="required" rows="5" cols="100"></textarea>
					</td>
					
				</tr>

				<tr class="TD_STYLE2" >
					<td width="10%" class="TD_STYLE1">
						职位状态
					</td>
					<td class="TD_STYLE2" colspan="7">
						<textarea name="majorPut.hrPutStatus" class="required" rows="5" cols="100"></textarea>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>