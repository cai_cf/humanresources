<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!-- 此页面为职位信息删除失败页面 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'modifyDelFail.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	
<!-- 导入jquery所需的类库 -->
	<script type="text/javascript" src="/humanresources/jquery/jquery-1.8.0.js"></script>
	<script type="text/javascript">
	var num=5;
		function time(){
			$("#link").html(num).css('color','red');
			num--;
			if(num == 0){
				window.location.href="/humanresources/recruit/selectMajorPut.action";
			}
		}	
		window.onload=setInterval("time()",1000);
	</script>
  </head>
  
  <body style="text-align: center;">
   	职位信息删除失败!<br/>
   	 <label id="link"></label>秒钟完成页面跳转,也可以点击此处 <a href="/humanresources/recruit/selectMajorPut.action" style="font-size: 12px;">点击这里完成跳转</a>
  </body>
</html>
