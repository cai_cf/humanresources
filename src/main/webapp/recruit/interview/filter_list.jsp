<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!-- 筛选列表成功页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>
		
		<!-- 导入 表格css-->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css" />
		
		<title>无标题文档</title>		
	</head>

	<body>
		<form method="post" action="interviewAction.do?methodName=Selectconfig">
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是:人力资源--招聘管理--面试管理--面试筛选--面试结果列表</font>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							<input type="button" value="返回" class="BUTTON_STYLE1" onClick="history.back();">
						</div>
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr class="TR_STYLE1">
					<td width="13%" class="TD_STYLE1">
						档案编号
					</td>
					<td width="13%" class="TD_STYLE1">
						姓名
					</td>
					<td width="14%" class="TD_STYLE1">
						职位名称 
					</td>
					<td width="14%" class="TD_STYLE1">
						面试时间
					</td>
					<td width="14%" class="TD_STYLE1">
						综合素质
					</td>
					<td width="7%" class="TD_STYLE1">
						筛选
					</td>
				</tr>
				<s:iterator value="#request.interView">
						<tr>
							<td class="TD_STYLE2">
								<s:property value="hrResume.hrResId"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrResume.hrPerName"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrResume.hrApplyMajor"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrInteRegisteTime"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrInteMultiQualityDegree"/>
							</td>
							<td class="TD_STYLE2">
								<a href="sInterviewById?id=${hrInteId}">筛选</a>
							</td>
						</tr>
					</s:iterator>
			</table>
			<p>&nbsp;&nbsp;总数：1例 &nbsp;&nbsp;&nbsp;当前第 1 页  &nbsp;&nbsp;&nbsp;共 1 页  &nbsp;&nbsp;&nbsp;跳到第 <input name=page type=text class=input1 size=1> 页&nbsp;&nbsp;<input type=image src="../../../images/go.bmp" width=18 height=18 border=0>
	</form>
	</body>
</html>