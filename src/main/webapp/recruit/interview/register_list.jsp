<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!-- 面试成功查询页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>
		
		<!-- 导入table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css" />
		
	<body>
	
		<form method="post" action="resumeAction.do?methodName=Selectconfig">
		
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是:人力资源--招聘管理--简历查询--简历筛选--简历筛选列表</font>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							<input type="button" value="返回" class="BUTTON_STYLE1" onclick="history.back();">
						</div>
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr class="TR_STYLE1">
					<td width="13%" class="TD_STYLE1">
						简历编号
					</td>
					<td width="13%" class="TD_STYLE1">
						姓名
					</td>
					<td width="11%" class="TD_STYLE1">
						性别
					</td>
					<td width="14%" class="TD_STYLE1">
						申请职位
					</td>
					<td width="14%" class="TD_STYLE1">
						专业
					</td>
					<td width="14%" class="TD_STYLE1">
						面试状态
					</td>
					<td width="14%" class="TD_STYLE1">
						登记
					</td>
				</tr>
			<s:iterator value="#request.resume">
						<tr>
							<td class="TD_STYLE2">
								<s:property value="hrResId"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrPerName"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrPerSex"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrApplyMajor"/>
							</td>
							<td class="TD_STYLE2">
								<s:property value="hrPerEducated"/>
							</td>
							
							<td class="TD_STYLE2">
							<!-- 如果审核通过的话，状态改为待面试 -->
							<c:if test="${hrPerStatus=='审核通过'}" >
							 待面试
							 </c:if>
							</td>
							
							<td class="TD_STYLE2">
								<a href="/humanresources/interview/getByIdInterview?id=${hrResId}">登记</a>
							</td>
						</tr>
			  </s:iterator>
			</table>
			<p>&nbsp;&nbsp;总数：1例 &nbsp;&nbsp;&nbsp;当前第 1 页  &nbsp;&nbsp;&nbsp;共 1 页  &nbsp;&nbsp;&nbsp;跳到第 <input name=page type=text class=input1 size=1> 页&nbsp;&nbsp;<input type=image src="../../../images/go.bmp" width=18 height=18 border=0>
		</form>
	</body>
</html>