<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!-- 简历筛选页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>
		
		<!-- 导入table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css" />
		
	</head>

	<body>
	<!-- form提交到模糊查询列表 -->
		<form method="post" action="/humanresources/resume/vagueResume.action">
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是:人力资源--招聘管理--简历查询--简历筛选</font>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							<input type="submit" value="开始" class="BUTTON_STYLE1">
						</div>
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr>
					<td width="20%" class="TD_STYLE1">根据专业进行模糊查询</td>
					<td class="TD_STYLE2">
						<input type="text" name="queryProperties.hrPerEducated" class="INPUT_STYLE1">
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1"><p>根据职位进行模糊查询</p>
				    <p>根据工作经验进行模糊查询</p></td>
					<td class="TD_STYLE2">
						<p>
						  <input type="text" name="queryProperties.hrApplyMajor" class="INPUT_STYLE1" id="date_start">
					  </p>
						<p>
					  <input type="text" name="queryProperties.hrPerJobRemark" class="INPUT_STYLE1" value=""  id="date_end">
					  </p></td>
				</tr>
			</table>
		</form>
	</body>
</html>