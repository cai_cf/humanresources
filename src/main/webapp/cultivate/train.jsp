<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>部门管理</title>
         <link rel="stylesheet" href="../css/table.css" type="text/css"></link>
         <script type="text/javascript">
         
          function toAdd(){
          document.getElementById("addUserInfo").style.display="block";
         }
         function none(){
          document.getElementById("addUserInfo").style.display="none";
         }
         function tiao(){
       
         var ye=document.getElementById("ye").value;
          if(isNaN(ye)){
         ye=1;
         }
        window.location.href="listtrains.action?fenYe.nowPage="+ye;
         }
         
    function cha(){
        var name=document.getElementById("name").value;
        var td=document.getElementById("td");
        if(name.length==0){
           td.innerHTML="培训项目名称不能为空";
           return false;
        } else{
        td.innerHTML="";
        return true;
        }
     
 }
   function cha1(){
        var name=document.getElementById("one").value;
        var td=document.getElementById("td1");
        if(name.length==0){
           td.innerHTML="开始时间不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
      function cha2(){
        var name=document.getElementById("two").value;
        var td=document.getElementById("td2");
        if(name.length==0){
           td.innerHTML="结束时间不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
          function cha3(){
        var name=document.getElementById("three").value;
        var td=document.getElementById("td3");
        if(name.length==0){
           td.innerHTML="金额不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
          function cha4(){
        var name=document.getElementById("four").value;
        var td=document.getElementById("td4");
        if(name.length==0){
           td.innerHTML="备注不能为空";
           return false;
        } else{
          td.innerHTML="";
          return true;
        }
         }
         function sub(){
          if(cha()&&cha1()&&cha2()&&cha3()&&cha4()){
          return true;
          }else{
          return false;
          }
         }
         </script>
  <style type="text/css">
   #addUserInfo {
	position:absolute;
	left:200px;
	top:58px;
	width:460px;
	height:240px;
	display:none;
	border: 1px solid #96E1A0;
}
span{
color:red;
}

         </style>
  </head>

	<body>
	<div id="addUserInfo">
	<form action="addtrain.action" method="post" onsubmit="return sub()">
  <table width="460" height="240" border="0" style="background-color:#CAFBCA;">
    <tr>
      <td height="10" colspan="2" align="center" class="TD_STYLE1">添加培训项目</td>
    </tr>
    <tr>
      <td width="32%" height="30" align="center" >培训项目名称：</td>
      <td width="68%" >
      <input name="tr.hrTraName" id="name" type="text" onblur="cha()"/>
      <span id="td"></span>
      </td>
    </tr>
    <tr>
      <td width="32%" height="30" align="center" >开始时间：</td>
      <td width="68%" >
        <input name="tr.hrTraBeginDate" id="one" type="text" onblur="cha1()"/>
        <span id="td1">时间格式(YYYY-MM-DD)</span>
      </td>
    </tr>
     <tr>
      <td width="32%" height="30" align="center" >结束时间：</td>
      <td width="68%" >
        <input name="tr.hrTraEndDate" id="two" type="text" onblur="cha2()"/>
        <span id="td2">时间格式(YYYY-MM-DD)</span>
      </td>
    </tr>
     <tr>
      <td width="32%" height="30" align="center" >培训金额：</td>
      <td width="68%" >
        <input name="tr.hrTraMoney" id="three" type="text" onblur="cha3()"/>
        <span id="td3"></span>
      </td>
    </tr>
     <tr>
      <td width="32%" height="30" align="center" >备注：</td>
      <td width="68%" >
        <input name="tr.hrTraDesc" id="four" type="text" onblur="cha4()"/>
        <span id="td4"></span>
      </td>
    </tr>
	<tr>
      <td height="30" colspan="2" align="center" >
        <input type="submit" value="增加"/>	 	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" name="Submit4" value="取消" onClick="none()"/>
      </td>
    </tr>
  </table>
  </form>
</div>
			<table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是:人力资源管理--培训管理--培训计划管理</font>
						</font>
					</td>
				</tr>
				<tr>
					<td align="right">
						<input type="button" value="添加" 
							onclick="toAdd();">
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				bordercolorlight=#848284 bordercolordark=#eeeeee
				class="TABLE_STYLE1">
				<tr>
					<td  class="TD_STYLE1">
						培训项目编号
					</td>
					<td class="TD_STYLE1">
						培训项目的名称
					</td>
					<td class="TD_STYLE1">
				          培训项目开始的时间
					</td>
					<td class="TD_STYLE1">
					培训项目结束的时间
					</td>
					<td class="TD_STYLE1">
					培训项目的预算开支
					</td>
					<td class="TD_STYLE1">
					培训项目的描述
					</td>
					<td class="TD_STYLE1"> 
					操作
					</td>
					
</tr>
					<c:forEach items="${arr}" var="ma" varStatus="i">
					<tr>
						<td class="TD_STYLE2">
						${i.index+1}
						</td>
						<td class="TD_STYLE2">
							${ma.hrTraName}
						</td >
						<td class="TD_STYLE2">
						${ma.hrTraBeginDate }
						</td>
						<td class="TD_STYLE2">
						${ma.hrTraEndDate}
						</td>
						<td class="TD_STYLE2">
						${ma.hrTraMoney}
						</td>
						<td class="TD_STYLE2">
						${ma.hrTraDesc}
						</td>
						<td width="20%">
						<a href="chatrains.action?id=${ma.hrTraId}">修改</a>
						<a href="deletetrain.action?id=${ma.hrTraId}">删除</a>
						</td>
						
						
					</tr>
					</c:forEach>
			</table>
			<p>&nbsp;&nbsp;总数：${fenYe.totalNums}例 &nbsp;&nbsp;&nbsp;当前第 ${fenYe.nowPage } 页  &nbsp;&nbsp;&nbsp;共 ${fenYe.totalPages} 页  &nbsp;&nbsp;&nbsp;跳到第 <input id="ye" type=text class=input1 size=1> 页&nbsp;&nbsp;<img src="../images/go.bmp"  onclick="tiao()" />
	</body>
</html>
    
