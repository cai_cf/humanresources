﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!-- 此页面为人力资源登录页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>
		<!-- 导入table.css页面 -->
		<link href="table.css" rel="stylesheet" />
		<!-- 导入jquery.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery-1.8.0.js"></script>
		<!-- 导入validator.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery.validate.js"></script>
		<!-- 导入login.js -->
		<script type="text/javascript" src="/humanresources/javascript/login.js"></script>
		<!-- 导入metadate.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery.metadata.js"></script>
		<!-- 导入messages_cn.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery.validate.messages_cn.js" charset="utf-8"></script>
		
		
		<STYLE type=text/css>BODY {
			FONT-SIZE: 12px; COLOR: #ffffff; FONT-FAMILY: 宋体
		}
		TD {
			FONT-SIZE: 12px; COLOR: #ffffff; FONT-FAMILY: 宋体
		}
		</STYLE>
		<script language="javascript">
		function loadimage(){
		document.getElementById("randImage").src = "image.jsp?"+Math.random();
		}
		</script> 
	
		
<BODY>


<DIV id=UpdatePanel1>
<DIV id=div1 
style="LEFT: 0px; POSITION: absolute; TOP: 0px; BACKGROUND-COLOR: #0066ff"></DIV>
<DIV id=div2 
style="LEFT: 0px; POSITION: absolute; TOP: 0px; BACKGROUND-COLOR: #0066ff"></DIV>


<DIV>&nbsp;&nbsp; </DIV>
<DIV>
<FORM id="login" action=/humanresources/user/login.action method=post>

<TABLE cellSpacing=0 cellPadding=0 width=900 align=center border=0>
  <TBODY>
  <TR>
    <TD style="HEIGHT: 105px"><IMG src="/humanresources/images/login_1.gif" 
  border=0></TD></TR>
  <TR>
    <TD background=/humanresources/images/login_2.jpg height=300>
      <TABLE height=300 cellPadding=0 width=900 border=0>
        <TBODY>
        <TR>
          <TD colSpan=2 height=35></TD></TR>
        <TR>
          <TD width=360></TD>
          <TD>
            <TABLE cellSpacing=0 cellPadding=2 border=0>
              <TBODY>
              <TR>
                <TD style="HEIGHT: 28px" width=80><s:text name="loginName"/>：</TD>
                <TD style="HEIGHT: 28px" width=150><INPUT id=txtName 
                  style="WIDTH: 130px" class="required" name="hrUserId"></TD>
              <!-- 此处为错误提示 -->
                <TD style="HEIGHT: 28px" width=370><SPAN 
                  id=RequiredFieldValidator3 
                  style="FONT-WEIGHT: bold; VISIBILITY: hidden; COLOR: white">请输入登录名</SPAN></TD></TR>
                <TR>
                <TD style="HEIGHT: 28px"><s:text name="loginPwd"/>：</TD>
                <TD style="HEIGHT: 28px"><INPUT id=txtPwd style="WIDTH: 130px" 
                  type=password name="hrUserPwd"></TD>
               	<!-- 此处为错误提示 -->
                <TD style="HEIGHT: 28px"><SPAN id=RequiredFieldValidator4 
                  style="FONT-WEIGHT: bold; VISIBILITY: hidden; COLOR: white">请输入密码</SPAN>
                </TD>
               </TR>
               <TR>
                <TD style="HEIGHT: 28px"><s:text name="validate"/>：</TD>
                <TD style="HEIGHT: 28px"><INPUT id=txtcode 
                  style="WIDTH: 130px"></TD>
                <TD style="HEIGHT: 28px"><img alt="code..." name="randImage" id="randImage" src="image.jsp" width="60" height="20" border="1" align="absmiddle"></TD>
                <td id="eri"></td></TR>
              <TR>
                <TD style="HEIGHT: 18px"></TD>
                <TD style="HEIGHT: 18px"></TD>
                <TD style="HEIGHT: 18px"><a href="javascript:loadimage();"><font class=pt95>看不清点我</font>
					</a></TD></TR>
              <TR>
                <TD></TD>
                <TD><INPUT id=btn 
                  style="BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px"  
                  type=image src="/humanresources/images/login_button.gif" name=btn> 
              </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD><IMG src="/humanresources/images/login_3.jpg" 
border=0></TD></TR></TBODY></TABLE></FORM></DIV></DIV>

</BODY></HTML>
