<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!-- 左面菜单页面 -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>
	
		<title>无标题文档</title>
		<!-- 导入table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css">
		

	</head>

	<script language="javascript">
	function plusORminus(span_name,pic_name)
	{
		span_name.style.display=='block'?pic_name.src='../images/plus22.gif':pic_name.src='../images/minus33.gif';
	}
	function noneORblock(span_name)
	{
		span_name.style.display=((span_name.style.display=='block')?'none':'block');
	}
</script>
	<body>
		<table width="100%" border="0">
			<tr>
				<th colspan="2" bgcolor="#1A98F1">
					<font size="+1" color="white">人力资源</font>
				</th>
			</tr>
			<!-- 客户化设置 -->
				<tr>
				<td width="2%">
					<a onClick="noneORblock(config)"><img src="../images/plus22.gif"
							onClick="plusORminus(config,config_pic)" id="config_pic">
					</a>
				</td>
				<td>
					客户化设置
				</td>
			</tr>
			<tr>
				<td width="2%"></td>
				<td width="98%">
					<span style="DISPLAY:none;" id="config">
						<table width="100%" border="0">
							<tr>
								<td width="2%">
									<a onClick="noneORblock(config_file)"><img
											src="../images/plus22.gif"
											onClick="plusORminus(config_file,config_file_pic)"
											id="config_file_pic">
									</a>
								</td>
								<td width="98%">
									管理设置
								</td>
							</tr>
							<tr>
								<td width="2%"></td>
								<td width="98%">
									<span style="DISPLAY:none;" id="config_file">
										<table width="100%" border="0">
											<tr>
												<td width="2%">
													<img src="../images/jt0.gif">
												</td>
												<td width="98%">
													<a href="/humanresources/department/listdepartment.action?fenYe.nowPage=1" target="mainFrame">部门设置</a>
												</td>
											</tr>
											<tr>
												<td>
													<img src="../images/jt0.gif">
												</td>
												<td>
													<a href="/humanresources/major/listmajor.action?fenYe.nowPage=1"
														target="mainFrame">职位设置</a>
												</td>
											</tr>
									
										</table> </span>
								</td>
							</tr>
							<tr>
								<td>
									<a onClick="noneORblock(config_salary)"><img
											src="../images/plus22.gif"
											onClick="plusORminus(config_salary,config_salary_pic)"
											id="config_salary_pic">
									</a>
								</td>
								<td>
									用户管理设置
								</td>
							</tr>
							<tr>
								<td width="2%"></td>
								<td width="98%">
									<span style="DISPLAY:none;" id="config_salary">
										<table width="100%" border="0">
											<tr>
												<td width="2%">
													<img src="../images/jt0.gif">
												</td>
												<td width="98%">
												<a href="listuser.action?fenYe.nowPage=1" target="mainFrame">	用户管理</a>
												</td>
											</tr>
										</table> </span>
								</td>
							</tr>
						</table> </span>
				</td>
			</tr>

			<!-- 人力资源档案管理设置 -->
			<tr>
				<td>
					<a onClick="noneORblock(human_file)"><img
							src="../images/plus22.gif"
							onClick="plusORminus(human_file,human_file_pic)"
							id="human_file_pic">
					</a>
				</td>
				<td>
					人力资源档案管理设置
				</td>
			</tr>
			<tr>
				<td width="2%"></td>
				<td width="98%">
					<span style="DISPLAY:none;" id="human_file">
						<table width="100%" border="0">
							<tr>
								<td width="2%">
									<img src="../images/jt0.gif">
								</td>
								<td width="98%">
									<a href="/humanresources/files/departmentAction_getAllDepartment.action" target="mainFrame">人力资源档案登记</a>
								</td>
							</tr>
					
							<tr>
								<td>
									<img src="../images/jt0.gif">
								</td>
								<td>
									<a href="/humanresources/filess/departmentAction_getAllDepartment.action" target="mainFrame">人力资源档案查询</a>
								</td>
							</tr>
							
							<tr>
												<td width="2%">
													<img src="../images/jt0.gif">
												</td>
												<td width="98%">
													<a href="../filesse/employeeAction_deleteEmployee.action?fileStatus=0" target="mainFrame">人力资源档案永久删除</a>
												</td>
											</tr>
											
							<tr>
												<td width="2%">
													<img src="../images/jt0.gif">
												</td>
												<td width="98%">
													<a href="../filessee/employeeAction_recoverEmployee.action?jobStatus=0" target="mainFrame">人力资源档案恢复</a>
												</td>
											</tr>
							
							
						</table> </span>
				</td>
			</tr>

			<!-- 薪酬标准管理 -->
			<tr>
				<td class="td_style">
					<a onClick="noneORblock(salary_standard)"><img
							src="../images/plus22.gif"
							onClick="plusORminus(salary_standard,salary_standard_pic)"
							id="salary_standard_pic">
					</a>
				</td>
				<td class="td_style">
					薪酬标准管理
				</td>
			</tr>
			<tr>
				<td width="2%"></td>
				<td width="98%">
					<span style="DISPLAY:none;" id="salary_standard">
						<table width="100%" border="0">
							<tr>
								<td width="2%">
									<img src="../images/jt0.gif">
								</td>
								<td width="98%">
									<a href="/humanresources/salary/listsalary.action?fenYe.nowPage=1" target="mainFrame">薪酬标准设置</a>
								</td>
							</tr>
						</table> </span>
				</td>
			</tr>

			<!-- 薪酬发放管理 -->
			<tr>
				<td class="td_style">
					<a onClick="noneORblock(salary)"><img src="../images/plus22.gif"
							onClick="plusORminus(salary,salary_pic)" id="salary_pic">
					</a>
				</td>
				<td class="td_style">
					薪酬发放管理
				</td>
			</tr>
			<tr>
				<td width="2%"></td>
				<td width="98%">
					<span style="DISPLAY:none;" id="salary">
						<table width="100%" border="0">
							<tr>
								<td width="2%">
									<img src="../images/jt0.gif">
								</td>
								<td width="98%">
									薪酬发放登记
								</td>
							</tr>
							<tr>
								<td>
									<img src="../images/jt0.gif">
								</td>
								<td>
									薪酬发放查询
								</td>
							</tr>
						</table> </span>
				</td>
			</tr>

			<!-- 薪酬标准管理 -->
			<tr>
				<td class="td_style">
					<a onClick="noneORblock(major_change)"><img
							src="../images/plus22.gif"
							onClick="plusORminus(major_change,major_change_pic)"
							id="major_change_pic">
					</a>
				</td>
				<td class="td_style">
					调动管理
				</td>
			</tr>
			<tr>
				<td width="2%"></td>
				<td width="98%">
					<span style="DISPLAY:none;" id="major_change">
						<table width="100%" border="0">
							<tr>
								<td width="2%">
									<img src="../images/jt0.gif">
								</td>
								<td width="98%">
								<!-- 调动登记 -->
								<a href="/humanresources/move/register_locate.action" target="mainFrame">
									调动登记
								</a>	
							</tr>
                            <tr>
								<td>
									<img src="../images/jt0.gif">
								</td>
								<td>
								<!-- 调动审核 -->
								<a href="/humanresources/move/selectRemoveMove.action" target="mainFrame">
									调动审核
								</a>
								</td>
							</tr>
							<tr>
								<td>
									<img src="../images/jt0.gif">
								</td>
								<!-- 调动查询 -->
								<td>
								<a href="/humanresources/move/moveRemoveMove.action" target="mainFrame">
									调动查询
								</a>
								</td>
							</tr>
						</table> </span>
				</td>
			</tr>

				<!-- 培训管理 -->
			<tr>
				<td class="td_style">
					<a onClick="noneORblock(training)"><img src="../images/plus22.gif"
							onClick="plusORminus(training,training_pic)" id="training_pic">
					</a>
				</td>
				<td class="td_style">
					培训管理
				</td>
			</tr>
			<tr>
				<td width="2%"></td>
				<td width="98%">
					<span style="DISPLAY:none;" id="training">
						<table width="100%" border="0">
							<tr>
								<td width="2%">
									<img src="../images/jt0.gif"> 
								</td>
								<td width="98%">
									<a href="/humanresources/cultivate/listtrains.action?fenYe.nowPage=1" target="mainFrame">培训项目管理</a>
								</td>
							</tr>
							<tr>
								<td width="2%">
									<img src="../images/jt0.gif">
								</td>
								<td width="98%">
									<a href="/humanresources/cultivate/listtrainpersonnel.action?fenYe.nowPage=1" target="mainFrame">培训记录</a>
								</td>
							</tr>
						
						</table> </span>
				</td>
			</tr>

		<!-- 激励管理 -->
			<tr>
				<td>
					<a onClick="noneORblock(bonus)"><img src="../images/plus22.gif"
							onClick="plusORminus(bonus,bonus_pic)" id="bonus_pic">
					</a>
				</td>
				<td>
					激励管理
				</td>
			</tr>
			<tr>
				<td width="2%"></td>
				<td width="98%">
					<span style="DISPLAY:none;" id="bonus">
						<table width="100%" border="0">
							<tr>
								<td width="2%">
									<img src="../images/jt0.gif">
								</td>
								<td width="98%">
									<a href="/humanresources/impel/listhrRewardStandard.action?fenYe.nowPage=1" target="mainFrame">激励标准管理</a>
								</td>
							</tr>
                            <tr>
								<td>
									<img src="../images/jt0.gif">
								</td>
								<td>
								<a href="/humanresources/impel/listhrReward.action?fenYe.nowPage=1" target="mainFrame">激励记录管理</a>
								</td>
							</tr>
						</table> </span>
				</td>
			</tr>

			<!-- 招聘管理 -->
			<tr>
				<td width="2%">
					<a onClick="noneORblock(engage)"><img src="../images/plus22.gif"
							onClick="plusORminus(engage,engage_pic)" id="engage_pic">
					</a>
				</td>
				<td>
					招聘管理
				</td>
			</tr>
			<tr>
				<td width="2%"></td>
				<td width="98%">
					<span style="DISPLAY:none;" id="engage">
						<table width="100%" border="0">
							<tr>
								<td width="2%">
									<a onClick="noneORblock(engage_major_release)"><img
											src="../images/plus22.gif"
											onClick="plusORminus(engage_major_release,engage_major_release_pic)"
											id="engage_major_release_pic">
									</a>
								</td>
								<td width="98%">
									职位发布管理
								</td>
							</tr>
							<tr>
								<td width="2%"></td>
								<td width="98%">
									<span style="DISPLAY:none;" id="engage_major_release">
										<table width="100%" border="0">
											<tr>
												<td width="2%">
													<img src="../images/jt0.gif">
												</td>
												<td width="98%">
												<!-- 职位发布登记 -->
													<a href="/humanresources/recruit/release/details.jsp"
														target="mainFrame">职位发布登记</a>
												</td>
											</tr>
											<tr>
												<td>
													<img src="../images/jt0.gif">
												</td>
												<td>
												<!-- 职位发布变更 -->
													<a href="/humanresources/recruit/selectMajorPut.action"
														target="mainFrame">职位发布变更</a>
												</td>
											</tr>
											<tr>
												<td>
													<img src="../images/jt0.gif">
												</td>
												<!-- 职位发布查询 -->
												<td>
													<a href="/humanresources/recruit/querytMajorPut.action"
														target="mainFrame">职位发布查询</a>
												</td>
											</tr>
										</table>  </span>
								</td>
							</tr>
							<tr>
								<td>
									<a onClick="noneORblock(engage_resume)"><img
											src="../images/plus22.gif"
											onClick="plusORminus(engage_resume,engage_resume_pic)"
											id="engage_resume_pic">
									</a>
								</td>
								<td>
									简历管理
								</td>
							</tr>
							<tr>
								<td width="2%"></td>
								<td width="98%">
									<span style="DISPLAY:none;" id="engage_resume">
										<table width="100%" border="0">
											<tr>
												<td width="2%">
													<img src="../images/jt0.gif">
												</td>
												<!-- 简历登记 -->
												<td width="98%">
													<a href="/humanresources/recruit/resume/register.jsp" target="mainFrame">简历登记</a>
												</td>
											</tr>
											<tr>
												<td>
													<img src="../images/jt0.gif">
												</td>
												<!-- 简历筛选 -->
												<td>
													<a href="/humanresources/recruit/resume/filter.jsp" target="mainFrame">简历筛选</a>
												</td>
											</tr>
											<tr>
												<td>
													<img src="../images/jt0.gif">
												</td>
												<!-- 有效简历查询 -->
												<td>
													<a href="/humanresources/recruit/resume/valid_filter.jsp" target="mainFrame">有效简历查询</a>
												</td>
											</tr>
											
											
										</table> </span>
								</td>
							</tr>
							<tr>
								<td>
									<a onClick="noneORblock(engage_rinterview)"><img
											src="../images/plus22.gif"
											onClick="plusORminus(engage_rinterview,engage_rinterview_pic)"
											id="engage_rinterview_pic">
									</a>
								</td>
								<td>
									面试管理
								</td>
							</tr>
							<tr>
								<td width="2%"></td>
								<td width="98%">
									<span style="DISPLAY:none;" id="engage_rinterview">
										<table width="100%" border="0">
											<tr>
												<td width="2%">
													<img src="../images/jt0.gif">
												</td>
												<!-- 面试结果登记 -->
												<td width="98%">
													<a href="/humanresources/interview/listInterview.action" target="mainFrame">面试结果登记</a>
												</td>
											</tr>
											<tr>
												<td>
													<img src="../images/jt0.gif">
												</td>
												<!-- 面试筛选 -->
												<td>
													<a href="/humanresources/interview/screenInterview.action" target="mainFrame">面试筛选</a>
												</td>
											
										</table> </span>
								</td>
							</tr>
							<tr>
								<td>
									<a onClick="noneORblock(engage_question)"><img
											src="../images/plus22.gif"
											onClick="plusORminus(engage_question,engage_question_pic)"
											id="engage_question_pic">
									</a>
								</td>
								<td>
									招聘考试题库管理
								</td>
							</tr>
							<tr>
								<td width="2%"></td>
								<td width="98%">
									<span style="DISPLAY:none;" id="engage_question">
										<table width="100%" border="0">
											<tr>
												<td width="2%">
													<img src="../images/jt0.gif">
												</td>
												<!-- 试题登记 -->
												<td width="98%">
													<a href="/humanresources/exam/selectExam.action" target="mainFrame">试题登记</a>
												</td>
											</tr>
											<tr>
												<td width="2%">
													<img src="../images/jt0.gif">
												</td>
												<!-- 试题查询 -->
												<td width="98%">
													<a href="/humanresources/exam/examSelect.action" target="mainFrame">试题查询</a>
												</td>
											</tr>
											<tr>
												<td width="2%">
													<img src="../images/jt0.gif">
												</td>
												<!-- 试题变更 -->
												<td width="98%">
													<a href="/humanresources/exam/examChangeSelect.action" target="mainFrame">试题变更</a>
												</td>
											</tr>
										</table></span>
								</td>
							</tr>
                            
						</table> </span>
				</td>
			</tr>
            <!-- 标准数据报表 -->
			<tr>
				<td>
					<a onClick="noneORblock(exports)"><img src="../images/plus22.gif"
							onClick="plusORminus(exports,exports_pic)" id="exports_pic">
					</a>
				</td>
				<td>
					考勤管理
				</td>
			</tr>
			<tr>
				<td width="2%"></td>
				<td width="98%">
					<span style="DISPLAY:none;" id="exports">
						<table width="100%" border="0">
							<tr>
								<td width="2%">
									<img src="../images/jt0.gif">
								</td>
								<td width="98%">
									<a href="report/excel_locate.html" target="mainFrame">查看考勤状况</a>
								</td>
							</tr>
							<tr>
								<td>
									<img src="../images/jt0.gif">
								</td>
								<td>
									<a href="exportfile.do?operate=toExport&method=pdf" target="mainFrame">查看考勤标准</a>
								</td>
							</tr>
						</table> </span>
				</td>
			</tr>
		</table>
	</body>
</html>
