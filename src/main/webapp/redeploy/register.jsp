<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
		<meta name="ProgId" content="FrontPage.Editor.Document">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
		<META content="Microsoft FrontPage 4.0" name=GENERATOR>
		
		<!-- 导入table.css -->
		<link rel="stylesheet" href="/humanresources/css/table.css" type="text/css"></link>		
		<!-- 导入jquery.js -->
		<script type="text/javascript" src="/humanresources/jquery/jquery-1.8.0.js"></script>
		<!-- 导入日期控件 -->
		<script language="javascript" type="text/javascript" src="/humanresources/My97DatePicker/WdatePicker.js"></script>
		<!-- 导入register.js -->
		<script type="text/javascript" src="/humanresources/javascript/redeploy/register.js"></script>
		
	</head>

	<body>
	<!-- form提交到掉动修改action -->
		<form method="post" action="/humanresources/move/updateMove.action">
		  <table width="100%">
				<tr>
					<td>
						<font color="#0000CC">您正在做的业务是:人力资源--调动管理--调动登记</font>
					</td>
				</tr>
				<tr>
					<td align="right">
						<input type="submit" value="提交" class="BUTTON_STYLE1">
						<input type="button" class="BUTTON_STYLE1" onClick="javascript:window.history.back();" value="返回">
					</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding=0 cellspacing=1
				class="TABLE_STYLE1">
				<tr height="21">
					<td class="TD_STYLE1" width="8%">
						档案编号
					</td>
					<td class="TD_STYLE2" width="10%">
						<input type="text" value="${move.hrEmpId}" class="INPUT_STYLE2" readonly="readonly">
					</td>					
					<td class="TD_STYLE1" width="12%">
						姓名
					</td>					
					<td class="TD_STYLE2" width="10%">
					<!-- 员工编号 -->
					<input type="hidden" name="move.hrEmployee.hrEmpId" value="${move.hrEmpId}">
					<input type="text" value="${move.hrEmpName}" class="INPUT_STYLE2" readonly="readonly">
					</td>
					<td class="TD_STYLE1" width="12%">原部门名称</td>
					<td class="TD_STYLE2" width="10%"><input type="text" name="move.hrDepOldName"  readonly="readonly" value="${move.hrDepartment.hrDepName}" class="INPUT_STYLE2">						
				  </td>
					<td class="TD_STYLE1" width="8%">原职位名称</td>
					<td class="TD_STYLE2" width="10%"><input type="text" name="move.hrMajOldName" readonly="readonly" value="${move.hrMajor.hrMajName}" class="INPUT_STYLE2">
					</td>
				</tr>
				<tr>
					<td class="TD_STYLE1" width="8%">
						新部门名称
					</td>
					<td class="TD_STYLE2" >
                    <select name="move.hrDepartment.hrDepId" class="INPUT_STYLE2" id="ndp">
                    	<s:iterator value="#request.department"><option value="${hrDepId}"><s:property value="#request.hrDepName"/></option></s:iterator>
                    </select>
					</td>
					<td class="TD_STYLE1" width="12%">新职位名称</td>
					<td class="TD_STYLE2" width="10%">
                    		<select name="move.hrMajor.hrMajId" class="INPUT_STYLE2" id="maj"> </select>
					</td>
					<td class="TD_STYLE1" width="12%">调动时间 </td>
					<td class="TD_STYLE2" width="10%"><input type="text" name="move.hrRemTime" onClick="WdatePicker()" id="Tdate" class="INPUT_STYLE2"></td>					
					<td class="TD_STYLE1" width="8%"></td>
					<td class="TD_STYLE2" width="10%"></td>
				</tr>
				<tr>
					<td class="TD_STYLE1">
						调动原因
					</td>
					<td colspan="7" class="TD_STYLE2">
						<textarea name="move.hrRemCause" rows="6" class="TEXTAREA_STYLE1"><s:property value="#request.move.hrRemCause"/></textarea>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>

