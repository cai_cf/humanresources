$(document).ready(
		function() {
			var $depart = $("#dep");
			var $major = $("#maj");
			$depart.change(function() {
				var departValue = $(this).val();
				$.post("/humanresources/movej/checkMove.action", {
					dpId : departValue
				}, function(data) {
					var va = "";
					for ( var i = 0; i < data.tranObject.length; i++) {
						va += "<option value='" + data.tranObject[i].id + "'>"
								+ data.tranObject[i].name + "</option>";
					}
					$major.html(va);
				}, "json");
			});
			var $sal = $("#sal");
			var $nsal = $("#nsal");
			$sal.change(function() {
				var salValue = $(this).val();
				$.post("/humanresources/movej/newSalary.action", {
					dpId : salValue
				}, function(data) {
					$nsal.val(data.dto.hrSalRental);
				}, "json");
			});
			$("#submit").click(function() {
				if ($("#rd1").attr("checked")) {
					$.post("/humanresources/movej/rd1.action",$("#form1").serialize() ,function(data){
						alert(data);
					},"json");
				}
				
				if ($("#rd2").attr("checked")) {
					var depId = $("#depId").val();
					$.post("/humanresources/movej/rd2.action", {
						dpId : depId
					}, function(data) {
						$.post("/humanresources/redeploy/rd2.jsp");
					}, "json");
				}
			});
		});