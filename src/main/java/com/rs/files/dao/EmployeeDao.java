package com.rs.files.dao;


import java.util.List;

import com.rs.common.dao.CommonDao;
import com.rs.common.entity.HrEmployee;
import com.rs.common.entity.QueryProperties;

public interface EmployeeDao<T> extends CommonDao<T> {
	public List<HrEmployee> getPropEmp(QueryProperties prop);
	public List<HrEmployee> deleteFileEmp(int status);
	public List<HrEmployee> updateJobEmp(int status);
	
	 //根据部门编号查询所有的员工
	  public  List<HrEmployee> getHrEmployeeBydepartment();
	  //单查用户是否存在
	  public HrEmployee  getentity(Integer id);
	  public List<HrEmployee> selectEmployee();
}
