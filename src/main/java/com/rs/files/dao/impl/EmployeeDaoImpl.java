package com.rs.files.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.rs.common.dao.impl.CommonDaoImpl;
import com.rs.common.entity.HrEmployee;
import com.rs.common.entity.QueryProperties;
import com.rs.files.dao.EmployeeDao;

public class EmployeeDaoImpl<T> extends CommonDaoImpl<T> implements EmployeeDao<T> {

	public List<HrEmployee> getPropEmp(QueryProperties p) {
		String hql = null;
		List<HrEmployee> arr = null;
		Object [] objs=new Object []{};
		try {
			List<Object> list = new ArrayList<Object>();
			hql = "from HrEmployee as e where 1=1";
			if (p != null) {
				boolean flag = false;
				if (p.getDepId() != 0) {
					hql += " and e.hrDepartment.hrDepId = ?";
					list.add(p.getDepId());
					flag = true;
				}
				if (p.getMajId() != 0) {
					hql += " and  e.hrMajor.hrMajId = ?";
					list.add(p.getMajId());
					flag = true;
				}

				if (p.getStartTime() != null && p.getEndTime() != null) {
					hql += " and e.hrEmpJobDater between ? and ?";
					list.add(p.getStartTime());
					list.add(p.getEndTime());
					flag = true;
				}
				if (flag){
					objs=list.toArray();
					arr = this.getHibernateTemplate().find(hql, objs);
				}
					
				else
					arr = this.getHibernateTemplate().find(hql);
			} else {
				arr = this.getHibernateTemplate().find(hql);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return arr;
	}

	public List<HrEmployee> deleteFileEmp(int status) {
		String hql = "from HrEmployee e where e.hrEmpFileStatus=?";
		return this.getHibernateTemplate().find(hql, status);
	}

	public List<HrEmployee> updateJobEmp(int status) {
		String hql = "from HrEmployee e where e.hrEmpJobStatus=?";
		return this.getHibernateTemplate().find(hql, status);
	}
	//根据部门编号查询所有的员工
	public List<HrEmployee> getHrEmployeeBydepartment() {
		// TODO Auto-generated method stub
		 List<HrEmployee>  arr=this.getHibernateTemplate().find("from HrEmployee");
		 if(arr.size()>0&arr!=null)
			 return arr;
		 else
		return null;
	}

	public HrEmployee getentity(Integer id) {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().get(HrEmployee.class, id);
	}
	public List<HrEmployee> selectEmployee() {
		return (List<HrEmployee>)getHibernateTemplate().find("from HrEmployee");
	}
}
