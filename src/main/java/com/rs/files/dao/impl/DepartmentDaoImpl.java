package com.rs.files.dao.impl;

import java.util.List;

import com.rs.common.dao.impl.CommonDaoImpl;
import com.rs.common.entity.HrDepartment;
import com.rs.files.dao.DepartmentDao;
/**
 * 
 * @author wanghao
 *
 * @param <T>
 */
public class DepartmentDaoImpl<T> extends CommonDaoImpl<T> implements DepartmentDao<T> {
	
	public List<HrDepartment> getAllDepartment() {
		return this.getHibernateTemplate().find("from HrDepartment");
	}

	public HrDepartment getByIdDep(int pid) {
		return this.getHibernateTemplate().get(HrDepartment.class,pid);
	}

}
