package com.rs.files.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.rs.common.entity.Dto;
import com.rs.common.entity.HrDepartment;
import com.rs.common.entity.HrEmployee;
import com.rs.common.entity.HrMajor;
import com.rs.common.entity.HrSalary;
import com.rs.common.entity.QueryProperties;
import com.rs.files.biz.DepartmentBiz;
import com.rs.files.biz.EmployeeBiz;
import com.rs.files.biz.MajorBiz;
import com.rs.files.biz.SalaryBiz;


public class EmployeeAction extends ActionSupport {

	private DepartmentBiz departmentBiz;
	private MajorBiz majorBiz;
	private SalaryBiz salaryBiz;
	private EmployeeBiz employeeBiz;
	private int pid;
	private int sid;
	private List<Dto> dtos;
	private int depId;
	private int majId;
	private int salId;
	private List<Dto> dts;
	private HrEmployee emp;
	private QueryProperties prop;

	private int count;
	private int empId;
	private int fileStatus;
	private int jobStatus;
	

	public int getFileStatus() {
		return fileStatus;
	}
	public void setFileStatus(int fileStatus) {
		this.fileStatus = fileStatus;
	}
	public int getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(int jobStatus) {
		this.jobStatus = jobStatus;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

	public QueryProperties getProp() {
		return prop;
	}
	public void setProp(QueryProperties prop) {
		this.prop = prop;
	}
	public int getDepId() {
		return depId;
	}
	public void setDepId(int depId) {
		this.depId = depId;
	}
	public int getMajId() {
		return majId;
	}
	public void setMajId(int majId) {
		this.majId = majId;
	}
	public int getSalId() {
		return salId;
	}
	public void setSalId(int salId) {
		this.salId = salId;
	}
	public void setEmployeeBiz(EmployeeBiz employeeBiz) {
		this.employeeBiz = employeeBiz;
	}
	public HrEmployee getEmp() {
		return emp;
	}
	public void setEmp(HrEmployee emp) {
		this.emp = emp;
	}
	public List<Dto> getDts() {
		return dts;
	}
	public void setDts(List<Dto> dts) {
		this.dts = dts;
	}
	public List<Dto> getDtos() {
		return dtos;
	}
	public void setDtos(List<Dto> dtos) {
		this.dtos = dtos;
	}
	public void setSalaryBiz(SalaryBiz salaryBiz) {
		this.salaryBiz = salaryBiz;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}

	// 进行数据传输对象
	private String dtoList;
	
	
	public String getDtoList() {
		return dtoList;
	}
	public void setDtoList(String dtoList) {
		this.dtoList = dtoList;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public void setDepartmentBiz(DepartmentBiz departmentBiz) {
		this.departmentBiz = departmentBiz;
	}
	public void setMajorBiz(MajorBiz majorBiz) {
		this.majorBiz = majorBiz;
	}
	//得到所有部门
	public String getAllDepartment(){
		Map<String, Object> request=(Map<String, Object>) ActionContext.getContext().get("request");
		List<HrDepartment> deps=departmentBiz.getAllDepartment();
		request.put("deps", deps);
		return SUCCESS;
	}
	
	//根据部门ID获得职位
	public String getByIdMajor(){

		List<HrMajor> pos=majorBiz.getAllMajor(pid);
		
		dtos=new ArrayList<Dto>();
		for (int i = 0; i < pos.size(); i++) {
			Dto d=new Dto();
			d.setId(pos.get(i).getHrMajId());
			d.setName(pos.get(i).getHrMajName());
	        dtos.add(d);
		}
		
		return SUCCESS;
	}
	//根据职位ID获得薪酬标准
	public String getByIdSalary(){
		List<HrSalary> sals=salaryBiz.getByIdSalary(sid);
		dts=new ArrayList<Dto>();
		for (int i = 0; i < sals.size(); i++) {
			Dto d=new Dto();
			d.setId(sals.get(i).getHrSalId());
			d.setName(sals.get(i).getHrSalName());
	        dts.add(d);
		}
		return SUCCESS;
	}
	
	//添加员工档案信息
	public String addEmployee(){
		try {
			HrDepartment hrDep=departmentBiz.getByIdDep(depId);
			HrMajor hrMaj=majorBiz.getByIdMaj(majId);
			HrSalary hrSal=salaryBiz.getByIdSal(salId);
			emp.setHrDepartment(hrDep);
			emp.setHrMajor(hrMaj);
			emp.setHrSalary(hrSal);
			emp.setHrEmpFileStatus(1);
			emp.setHrEmpCheckStatus(1);
			emp.setHrEmpJobStatus(1);
			employeeBiz.addEmployee(emp);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	//多条件组合查询
	public String selectEmployee(){
		try {
			Map<String, Object> request=(Map<String, Object>) ActionContext.getContext().get("request");
			List<HrEmployee> empList=employeeBiz.getPropEmp(prop);
			request.put("empList", empList);
			count=empList.size();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	public String updateEmployee(){
		try {
			employeeBiz.updateEmployee(empId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	public String deleteEmployee(){
		try {
			Map<String, Object> request=(Map<String, Object>) ActionContext.getContext().get("request");
			List<HrEmployee> empLis=employeeBiz.deleteFileEmp(fileStatus);
			request.put("empLis", empLis);
			count=empLis.size();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	public String recoverEmployee(){
		try {
			Map<String, Object> request=(Map<String, Object>) ActionContext.getContext().get("request");
			List<HrEmployee> empLi=employeeBiz.updateJobEmp(jobStatus);
			request.put("empLi", empLi);
			count=empLi.size();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	public String recoversEmployee(){
		employeeBiz.updatesEmployee(empId);
		return SUCCESS;
	}
	
	public String deletesEmployee(){
		try {
			employeeBiz.deleteEmp(empId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	//人力资源档案变更查询
	public String selEmployee(){
		emp=employeeBiz.selEmployee(empId);
		return SUCCESS;
	}
	public String upEmployee(){
		HrDepartment hrDep=departmentBiz.getByIdDep(depId);
		HrMajor hrMaj=majorBiz.getByIdMaj(majId);
		HrSalary hrSal=salaryBiz.getByIdSal(salId);
		emp.setHrDepartment(hrDep);
		emp.setHrMajor(hrMaj);
		emp.setHrSalary(hrSal);
		employeeBiz.upEmployee(emp);
		return SUCCESS;
	}
}
