package com.rs.files.biz;

import java.util.List;

import com.rs.common.entity.HrMajor;

public interface MajorBiz {

	public List<HrMajor> getAllMajor(int pid);
	public HrMajor getByIdMaj(int mid);
}
