package com.rs.files.biz.impl;

import java.util.List;

import com.rs.common.entity.HrMajor;
import com.rs.files.biz.MajorBiz;
import com.rs.files.dao.MajorDao;

public class MajorBizImpl implements MajorBiz {
	private static final Class HrMajor = null;
	private MajorDao majorDao;
	
	public void setMajorDao(MajorDao majorDao) {
		this.majorDao = majorDao;
	}

	public List<HrMajor> getAllMajor(int pid) {
		return majorDao.getAllMajor(pid);
	}

	public HrMajor getByIdMaj(int mid) {
		return majorDao.getByIdMaj(mid);
	}

}
