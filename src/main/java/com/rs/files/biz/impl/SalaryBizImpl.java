package com.rs.files.biz.impl;

import java.util.List;

import com.rs.common.entity.HrSalary;
import com.rs.files.biz.SalaryBiz;
import com.rs.files.dao.SalaryDao;

public class SalaryBizImpl implements SalaryBiz {

	private static final Class HrSalary = null;
	private SalaryDao salaryDao;
	public void setSalaryDao(SalaryDao salaryDao) {
		this.salaryDao = salaryDao;
	}
	public List<HrSalary> getByIdSalary(int sid) {
		return salaryDao.getByIdSalary(sid);
	}
	public HrSalary getByIdSal(int sid) {
		
		return  (HrSalary) salaryDao.getByIdSal(sid);
	}
	
	

	
}
