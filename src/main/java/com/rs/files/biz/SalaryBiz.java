package com.rs.files.biz;

import java.util.List;

import com.rs.common.entity.HrSalary;

public interface SalaryBiz {

	public List<HrSalary> getByIdSalary(int sid);
	public HrSalary getByIdSal(int sid);
}
