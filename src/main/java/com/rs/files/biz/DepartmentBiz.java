package com.rs.files.biz;

import java.util.List;

import com.rs.common.entity.HrDepartment;

public interface DepartmentBiz {

	public List<HrDepartment> getAllDepartment();
	public HrDepartment getByIdDep(int pid);
	
}
