package com.rs.user.action;

import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrUser;
import com.rs.user.biz.UserBiz;

public class UserAction extends ActionSupport {

	/*
	 * 作者:王运发
	 * 时间 2013年11月5日 16:09:51
	 */
	private UserBiz userBiz;
	private String hrUserPwd;
	private int hrUserId;
	private List<HrUser> arr;
	private FenYe fenYe;
	private HrUser user;
	private Integer id;

	public void setId(Integer id) {
		this.id = id;
	}

	public HrUser getUser() {
		return user;
	}

	public void setUser(HrUser user) {
		this.user = user;
	}

	public FenYe getFenYe() {
		return fenYe;
	}

	public void setFenYe(FenYe fenYe) {
		this.fenYe = fenYe;
	}

	public List<HrUser> getArr() {
		return arr;
	}

	public void setHrUserPwd(String hrUserPwd) {
		this.hrUserPwd = hrUserPwd;
	}

	public void setHrUserId(int hrUserId) {
		this.hrUserId = hrUserId;
	}

	public void setUserBiz(UserBiz userBiz) {
		this.userBiz = userBiz;
	}

	public String login() {
		try {
			HrUser user=userBiz.loginUser(hrUserId, hrUserPwd);
			if(user!=null){
				Map session=ActionContext.getContext().getSession();
				session.put("user", user);
				return SUCCESS;
			}else{
				
				return INPUT;
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	public String add(){
		try {
		userBiz.addHrUser(user);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	public String update(){
		try {
			user=userBiz.getUserById(id);
		    userBiz.updateHrUserById(user);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	public String delete(){
		try {
		  userBiz.deleteHrUserById(id);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	public String list(){
		try {
		 arr=userBiz.getUsers(fenYe);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
}