package com.rs.impel.biz.impl;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrRewardStandard;
import com.rs.impel.biz.HrRewardStandardBiz;
import com.rs.impel.dao.HrRewardStandardDao;

public class HrRewardStandardBizImpl implements HrRewardStandardBiz {

	private  HrRewardStandardDao hrRewardStandardDao;
	
	
	public void setHrRewardStandardDao(HrRewardStandardDao hrRewardStandardDao) {
		this.hrRewardStandardDao = hrRewardStandardDao;
	}
	//增加激励标准
	public void addHrRewardStandard(HrRewardStandard rs) {
		// TODO Auto-generated method stub
		hrRewardStandardDao.addEntity(rs);
	}
	//全查激励标准
	public List<HrRewardStandard> getAllHrRewardStandard() {
		// TODO Auto-generated method stub
		return  hrRewardStandardDao.getAllHrRewardStandard();
	}
	//分页查询激励标准
	public List<HrRewardStandard> getHrRewardStandard(FenYe fenYe) {
		// TODO Auto-generated method stub
		return hrRewardStandardDao.getList(HrRewardStandard.class, fenYe);
	}

	//单查激励标准
	public HrRewardStandard getHrRewardStandardByid(Integer id) {
		// TODO Auto-generated method stub
		return (HrRewardStandard) hrRewardStandardDao.getEntityById(HrRewardStandard.class, id);
	}
	//修改激励标准
	public void updateHrRewardStandard(HrRewardStandard rs) {
		// TODO Auto-generated method stub
		hrRewardStandardDao.updateEntity(rs);
	}
	//删除
	public void deleteHrRewardStandard(Integer id) {
		// TODO Auto-generated method stub
		hrRewardStandardDao.deleteEntityById(HrRewardStandard.class, id);
	}

}
