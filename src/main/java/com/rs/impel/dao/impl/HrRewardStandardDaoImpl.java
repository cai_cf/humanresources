package com.rs.impel.dao.impl;

import java.util.List;

import com.rs.common.dao.impl.CommonDaoImpl;
import com.rs.common.entity.HrRewardStandard;
import com.rs.impel.dao.HrRewardStandardDao;

public class HrRewardStandardDaoImpl extends CommonDaoImpl implements HrRewardStandardDao {
	//全查所有的激励标准
	public  List<HrRewardStandard> getAllHrRewardStandard() {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().find("from HrRewardStandard");
	}

}
