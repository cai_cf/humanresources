package com.rs.impel.action;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrRewardStandard;
import com.rs.impel.biz.HrRewardStandardBiz;

public class HrRewardStandardAction extends ActionSupport {
	private HrRewardStandardBiz hrRewardStandardBiz;
	private HrRewardStandard rs;
	private Integer id;
	private List<HrRewardStandard> arr;
	private FenYe fenYe;
	
	
	
	

	public void setId(Integer id) {
		this.id = id;
	}
	public FenYe getFenYe() {
		return fenYe;
	}
	public void setFenYe(FenYe fenYe) {
		this.fenYe = fenYe;
	}
	public List<HrRewardStandard> getArr() {
		return arr;
	}
	public HrRewardStandard getRs() {
		return rs;
	}
	public void setRs(HrRewardStandard rs) {
		this.rs = rs;
	}
	public void setHrRewardStandardBiz(HrRewardStandardBiz hrRewardStandardBiz) {
		this.hrRewardStandardBiz = hrRewardStandardBiz;
	}
	public String add(){
		try {
			hrRewardStandardBiz.addHrRewardStandard(rs);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	public String update(){
		try {
			hrRewardStandardBiz.updateHrRewardStandard(rs);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
public String cha(){
		try {
			rs= hrRewardStandardBiz.getHrRewardStandardByid(id);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	public String list(){
		try {
			arr=  hrRewardStandardBiz.getHrRewardStandard(fenYe);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	public String delete(){
		try {
			 hrRewardStandardBiz.deleteHrRewardStandard(id);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	
}
