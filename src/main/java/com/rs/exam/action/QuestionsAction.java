package com.rs.exam.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.rs.common.entity.HrQuestions;
import com.rs.common.entity.HrQuestionsKind;
import com.rs.exam.biz.QuestionsBiz;
import com.rs.exam.dao.QueryProperties;

/**
 *  试题管理模块
 * @author 贾志新
 *
 */
public class QuestionsAction extends ActionSupport implements RequestAware {

	// 调用biz层方法
	private QuestionsBiz questionsBiz;
	
	// 对象注入让模型层得到
	private HrQuestions question;
	
	// 实用类进行查询
	private QueryProperties queryProperties;
	
	// 此属性为进行根据id进行查询专用
	private Integer id;
	
	// 让页面的id进行注入专用
	public void setId(Integer id) {
		this.id = id;
	}
	// 进行get封装
	public QueryProperties getQueryProperties() {
		return queryProperties;
	}
	// 进行set封装
	public void setQueryProperties(QueryProperties queryProperties) {
		this.queryProperties = queryProperties;
	}

	// 进行对象set封装
	public void setQuestion(HrQuestions question) {
		this.question = question;
	}
	
	// 进行对象get封装
	public HrQuestions getQuestion() {
		return question;
	}
	//设置通用Map request
	Map<String,Object> request;

	// 进行通用设置
	public void setRequest(Map<String, Object> request) {
		this.request=request;
	}

	// 进行biz层方法实现
	public void setQuestionsBiz(QuestionsBiz questionsBiz) {
		this.questionsBiz = questionsBiz;
	}
	
	// 添加方法
	public String addQuestions(){
		try {
			/**
			 * 此处写try catch目的
			 * 如果添加成功返回添加成功信息页面
			 * 如果添加失败返回操作有误页面
			 */
			questionsBiz.addQuestions(question); //根据对象注入进行添加
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	
	// 查询试题种类方法
	public String selectExam(){
		try {
			/**
			 * 此处写try catch目的
			 * 如果查询成功返回到成功页面
			 * 如果查询失败返回到失败页面
			 */
			List<HrQuestionsKind> questionsKind=questionsBiz.selectKindName();
			request.put("questionsKind", questionsKind);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	
	// 试题查询定义方法
	public String examSelect(){
		try {
			// 试题查询成功跳转到试题成功页面
			// 试题失败跳转到试题查询失败页面
			List<HrQuestionsKind> questionsKind=questionsBiz.selectKindName();
			request.put("questionsKind", questionsKind);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 试题查询根据查询条件
	public String termExam(){
		try {
			/**
			 * 此处写try catch目的
			 * 如果成功转到查询列表页面
			 * 如果失败转到查询失败页面
			 */
			List<HrQuestions> selectByName = questionsBiz.selectByName(queryProperties);
			request.put("selectByName", selectByName);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 根据id查询试题详细
	public String detailsExam(){
		try {
			/**
			 * 根据此id进行查询
			 * 如果成功跳转到试题详细页面
			 * 如果失败跳转到试题查询失败页面
			 */
			HrQuestions getQuestionsById = questionsBiz.getQuestionsById(id);
			request.put("getQuestionsById", getQuestionsById);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 试题变更查询
	public String examChangeSelect(){
		try {
			// 调用biz
			List<HrQuestions> question=questionsBiz.selectAll();
			request.put("question", question);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 根据id进行试题变更查询方法
	public String examByIdUpdate(){
		try {
			/**
			 * 此try catch
			 * 如果成功跳转到成功页面
			 * 如果失败跳转到失败页面
			 */
			HrQuestions question = questionsBiz.getQuestionsById(id);
			request.put("question", question);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 试题修改
	public String updateQuestions(){
		try {
			/**
			 * try catch
			 * 如果成功跳转到修改成功页面
			 * 如果修改失败跳转到修改失败页面
			 */
			questionsBiz.updateExam(question);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	
}
