package com.rs.interview.biz.impl;

import java.util.List;

import com.rs.common.entity.HrInterview;
import com.rs.common.entity.HrResume;
import com.rs.interview.biz.InterViewBiz;
import com.rs.interview.dao.InterViewDao;

public class InterViewBizImpl implements InterViewBiz {

	// 根据spring注入机制进行以来注入
	private InterViewDao interViewDao;
	// 进行set 封装
	public void setInterViewDao(InterViewDao interViewDao) {
		this.interViewDao = interViewDao;
	}
	// 调用Dao层查询方法
	public List<HrResume> listInterView() {
		return (List<HrResume>) interViewDao.listInterView();
	}
	// 调用Dao层单查方法
	public HrResume getById(Integer id) {
		return interViewDao.getById(id);
	}
	// 调用Dao层添加方法
	public void addInterView(HrInterview interview) {
		interview.setHrInteStatus("待面试");
		interViewDao.addInterView(interview);
	}
	//　调用Dao层进行修改方法实现
	public void updateInterView(HrResume resume) {
		interViewDao.updateInterView(resume);
	}
	// 调用Dao层面试查寻方法
	public List<HrInterview> interviewList() {
		return (List<HrInterview>)interViewDao.interviewList();
	}
	// 调用Dao层实现修改方法
	public void upInterView(HrInterview view) {
		interViewDao.upInterView(view);
	}
	// 调用Dao层实现单个面试查询方法
	public HrInterview sInterviewById(Integer id) {
		return interViewDao.sInterviewById(id);
	}
	// 调用Dao层实现所有建议录用方法
	public List<HrInterview> selectEmploy() {
		return (List<HrInterview>)interViewDao.selectEmploy();
	}
}
