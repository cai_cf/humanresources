package com.rs.interview.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.rs.common.entity.HrInterview;
import com.rs.common.entity.HrResume;
import com.rs.interview.biz.InterViewBiz;
/**
 * 面试管理模块
 * @author 贾志新
 *
 */
public class InterViewAction extends ActionSupport implements RequestAware{

	// 调用biz层进行依赖注入
	private InterViewBiz interViewBiz;
	
	// 注入对象
	private HrInterview interView;
	
	// 设置对象的get 方法
	public HrInterview getInterView() {
		return interView;
	}
	// 设置对象的 set 方法
	public void setInterView(HrInterview interView) {
		this.interView = interView;
	}
	// 调用页面传过来的id
	private Integer id;
	
	// 根据spring的依赖注入给id进行注入
	public void setId(Integer id) {
		this.id = id;
	}
	// 对biz方法进行封装
	public void setInterViewBiz(InterViewBiz interViewBiz) {
		this.interViewBiz = interViewBiz;
	}
	// 实现通用设置
	private Map<String,Object> request;
	// 设置通用
	public void setRequest(Map<String, Object> request) {
		this.request=request;
	}
	
	// 进行简历查询方法
	public String listInterview(){
		try {
			List<HrResume> resume = interViewBiz.listInterView();
			request.put("resume", resume);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	
	// 根据ID查询面试
	public String getByIdInterview(){
		try {
			/**
			 * 此处写try catch目的
			 * 如果成功的话,进入登记页面
			 * 如果失败的话，进入失败页面
			 */
			HrResume resume = interViewBiz.getById(id);
			request.put("resume", resume);
			return  SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	
	// 面试表添加
	public String addInterView(){
		try {
			/**
			 * 此处写try catch目的
			 * 如果添加成功返回面试添加成功页面
			 * 如果添加失败返回面试添加失败页面
			 */
			interViewBiz.addInterView(interView);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 面试筛选
	public String screenInterview(){
		try {
			/**
			 * 此处写try catch目的
			 * 如果筛选成功跳转到筛选成功列表页面
			 * 如果筛选失败跳转到筛选失败页面
			 */
			List<HrInterview> interView=interViewBiz.interviewList();
			request.put("interView", interView);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 根据id查询筛选信息
	public String sInterviewById(){
		try {
			/**
			 * 此处写try catch目的
			 * 如果成功返回成功页面
			 * 如果失败返回失败页面
			 */
			HrInterview view=interViewBiz.sInterviewById(id);
			// 将查询出信息放入集合中
			request.put("view", view);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 修改职位状态
	public String upInterView(){
		try {
			/**
			 * 此处写try catch目的
			 * 如果修改成功跳转到成功页面
			 * 如果修改失败跳转到失败页面
			 */
			interViewBiz.upInterView(interView);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 修改成功职位查询
	public String selectEmploy(){
		try {
			List<HrInterview> interview=interViewBiz.selectEmploy();
			request.put("interview", interview);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
}
