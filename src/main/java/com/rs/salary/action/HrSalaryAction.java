package com.rs.salary.action;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrMajor;
import com.rs.common.entity.HrSalary;
import com.rs.major.biz.HrMajorBiz;
import com.rs.salary.biz.HrSalaryBiz;

public class HrSalaryAction extends ActionSupport {
	
	/**
	 * @author 王运发
	 * 时间:2013年11月8日 14:02:19
	 */
      private HrSalaryBiz hrSalaryBiz;
      private HrSalary  salary;
      private List<HrMajor> list;
      private FenYe fenYe;
      private List<HrSalary> arr;
      private Integer id;
      private HrMajorBiz hrMajorBiz;
      
      
	
	public void setId(Integer id) {
		this.id = id;
	}





	public void setHrMajorBiz(HrMajorBiz hrMajorBiz) {
		this.hrMajorBiz = hrMajorBiz;
	}





	public List<HrSalary> getArr() {
		return arr;
	}


	public void setArr(List<HrSalary> arr) {
		this.arr = arr;
	}


	public FenYe getFenYe() {
		return fenYe;
	}


	public void setFenYe(FenYe fenYe) {
		this.fenYe = fenYe;
	}


	public List<HrMajor> getList() {
		return list;
	}




	public HrSalary getSalary() {
		return salary;
	}


	public void setSalary(HrSalary salary) {
		this.salary = salary;
	}


	public void setHrSalaryBiz(HrSalaryBiz hrSalaryBiz) {
		this.hrSalaryBiz = hrSalaryBiz;
	}


	public String add(){
		try {
		
			HrMajor ma=hrMajorBiz.getHrMahorById(id);
			salary.setHrMajor(ma);
	        hrSalaryBiz.addHrSalary(salary);
		return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	public String list(){
		try {
			list=hrMajorBiz.getAllHrMajor();
		   arr=hrSalaryBiz.getHrSalary(fenYe);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	public String dan(){
		try {
			salary=hrSalaryBiz.getSalaryById(id);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	public String update(){
		try {
			hrSalaryBiz.updateHrSalary(salary);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	public String delete(){
		try {
			hrSalaryBiz.deleteHrSalary(id);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	
}
