package com.rs.salary.biz.impl;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrSalary;
import com.rs.salary.biz.HrSalaryBiz;
import com.rs.salary.dao.HrSalaryDao;

/**
 *@author onlyloveyoubaby
 *
 *
 * 作者:王运发
 * 时间:2013年11月6日 17:14:44
 */

 

public class HrSalaryBizImpl implements HrSalaryBiz {
	private HrSalaryDao hrSalaryDao;
	public void setHrSalaryDao(HrSalaryDao hrSalaryDao) {
		this.hrSalaryDao = hrSalaryDao;
	}

	// 增加工资标准
	public void addHrSalary(HrSalary sa) {
		// TODO Auto-generated method stub
           hrSalaryDao.addEntity(sa);
	}

	// 删除工资标准
	public void deleteHrSalary(Integer id) {
	   	// TODO Auto-generated method stub
            hrSalaryDao.deleteEntityById(HrSalary.class, id);      
	}

	// 分页查询
	public List<HrSalary> getHrSalary(FenYe fenYe) {
		// TODO Auto-generated method stub
		return hrSalaryDao.getList(HrSalary.class, fenYe);
	}

	// 修改工资标准
	public void updateHrSalary(HrSalary sa) {
		// TODO Auto-generated method stub
		hrSalaryDao.updateEntity(sa);

	}
//全查
	public List<HrSalary> getAllSalary() {
		// TODO Auto-generated method stub
		return hrSalaryDao.getAllSalary();
	}
//单查
	public HrSalary getSalaryById(Integer id) {
		// TODO Auto-generated method stub
		return (HrSalary) hrSalaryDao.getEntityById(HrSalary.class, id);
	}

}
