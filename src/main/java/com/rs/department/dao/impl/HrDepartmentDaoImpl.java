package com.rs.department.dao.impl;

import java.util.List;

import com.rs.common.dao.impl.CommonDaoImpl;
import com.rs.common.entity.HrDepartment;
import com.rs.department.dao.HrDepartmentDao;

/*
 * 作者： 王运发
 * 时间： 2013-10-30  14:40
 */
public class HrDepartmentDaoImpl extends CommonDaoImpl implements HrDepartmentDao{

	//全查所有的部门
	public List<HrDepartment> getAllHrDepartment() {
		// TODO Auto-generated method stub
		return this.getHibernateTemplate().find("from HrDepartment");
	}

	
}
