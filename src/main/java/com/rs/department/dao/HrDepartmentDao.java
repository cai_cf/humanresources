package com.rs.department.dao;

import java.util.List;

import com.rs.common.dao.CommonDao;
import com.rs.common.entity.HrDepartment;

/*
 * 作者： 王运发
 * 时间： 2013-10-30 14:40
 */
public interface HrDepartmentDao extends CommonDao {
	//查询所有的部门
	public List<HrDepartment>getAllHrDepartment();

}
