package com.rs.major.dao.impl;

import java.io.Serializable;
import java.util.List;

import com.rs.common.dao.impl.CommonDaoImpl;
import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrMajor;
import com.rs.major.dao.HrMajorDao;

/*
 * 作者: 王运发
 * 时间:2013-11-05 14:09
 */
public class HrMajorDaoImpl extends CommonDaoImpl implements HrMajorDao {

	//得到所有的职位
	public List<HrMajor> getAllHrMajor() {
		// TODO Auto-generated method stub
		return  this.getHibernateTemplate().find("from HrMajor");
	}
}
