package com.rs.major.dao;

import java.util.List;

import com.rs.common.dao.CommonDao;
import com.rs.common.entity.HrMajor;

/*
 * 作者: 王运发
 * 时间:2013-11-05 14:09
 */
public interface HrMajorDao extends CommonDao {

	//全查所有的职位
	public List<HrMajor> getAllHrMajor();
}
