package com.rs.major.action;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrDepartment;
import com.rs.common.entity.HrMajor;
import com.rs.department.biz.HrDepartmentBiz;
import com.rs.major.biz.HrMajorBiz;

public class HrMajorAction extends ActionSupport {
	/*
	 * 作者:王运发
	 * 时间 2013年11月5日 16:09:51
	 */
	private  HrMajorBiz hrMajorBiz;
	private FenYe fenYe;
	private HrMajor ma;
	private List<HrMajor> arr;
	private HrDepartmentBiz hrDepartmentBiz;
	private List<HrDepartment> list;
	private Integer id;
	
	
	public void setId(Integer id) {
		this.id = id;
	}
	public List<HrDepartment> getList() {
		return list;
	}
	public void setHrDepartmentBiz(HrDepartmentBiz hrDepartmentBiz) {
		this.hrDepartmentBiz = hrDepartmentBiz;
	}
	public FenYe getFenYe() {
		return fenYe;
	}
	public void setFenYe(FenYe fenYe) {
		this.fenYe = fenYe;
	}
	public List<HrMajor> getArr() {
		return arr;
	}
	public HrMajor getMa() {
		return ma;
	}
	public void setMa(HrMajor ma) {
		this.ma = ma;
	}
	
	public void setHrMajorBiz(HrMajorBiz hrMajorBiz) {
		this.hrMajorBiz = hrMajorBiz;
	}
	public String add(){
		try {
		  HrDepartment de=hrDepartmentBiz.getHrDepartmentById(id);
		  ma.setHrDepartment(de);
		  hrMajorBiz.addHrMajor(ma);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	public String list(){
		try {
			list=hrDepartmentBiz.getAllHrDepartment();
			arr=hrMajorBiz.getAllHrMajor(fenYe);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}


}
