package com.rs.move.dao;

public class DataTranObject {

	// 进行数据传输类
	// 数据传输id
	private int id;
	// 数据传输name
	private String name;
	
	// 数据传输有参构造方法
	public DataTranObject(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	// 数据传输无参构造方法
	public DataTranObject() {
	}
	/**
	 * 进行 get set 封装
	 */
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
