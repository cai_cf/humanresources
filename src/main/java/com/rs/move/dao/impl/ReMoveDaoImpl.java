package com.rs.move.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.rs.common.entity.HrDepartment;
import com.rs.common.entity.HrEmployee;
import com.rs.common.entity.HrMajor;
import com.rs.common.entity.HrRemove;
import com.rs.common.entity.HrSalary;
import com.rs.move.dao.QueryProperties;
import com.rs.move.dao.ReMoveDao;

public class ReMoveDaoImpl extends HibernateDaoSupport implements ReMoveDao {

	// 实现调动方法
	public List<HrRemove> removeList() {
		return (List<HrRemove>) getHibernateTemplate().find("from HrRemove");
	}

	// 实现单个对象方法
	public HrEmployee move(Integer id) {
		return getHibernateTemplate().get(HrEmployee.class, id);
	}

	// 实现查询所有职位方法
	public List<HrMajor> selectMajor(Integer id) {
		return (List<HrMajor>) getHibernateTemplate().find(
				"from HrMajor hm where hm.hrDepartment.hrDepId=?", id);
	}

	// 实现查询所有部门方法
	public List<HrDepartment> selectDepartment() {
		return (List<HrDepartment>) getHibernateTemplate().find(
				"from HrDepartment");
	}

	// 实现修改调动信息方法
	public void updateRemove(HrRemove move) {
		getHibernateTemplate().update(move);
	}

	// 实现添加职位方法
	public void addRemove(HrRemove move) {
		getHibernateTemplate().save(move);
	}

	// 实现修改档案信息方法
	public void updateEmployee(HrEmployee employee) {
		getHibernateTemplate().update(employee);
	}

	// 根据id查询单个员工id
	public HrEmployee employeeGetId(Integer id) {
		return getHibernateTemplate().get(HrEmployee.class, id);
	}

	// 实现条件待审核信息
	public List<HrRemove> selectRemove() {
		return (List<HrRemove>) getHibernateTemplate().find(
				"from HrRemove hr where hr.hrRemStatus='待审核'");
	}

	// 实现模糊查询方法
	public List<HrEmployee> blurQuery(QueryProperties qp) {
		List<HrEmployee> employee = null;
		// 如果部门，职位，起始时间，结束时间为空进行全查

		if (qp.getDepId().equals("") && qp.getMajId().equals("")
				&& qp.getStartTime().equals("") && qp.getEndTime().equals("")) {
			employee = getHibernateTemplate().find("from HrEmployee");
			return employee;
		}
		// 如果部门，职位，起始时间，结束时间不为空进行查询
		else if (!qp.getDepId().equals("") && !qp.getMajId().equals("")
				&& !qp.getStartTime().equals("") && !qp.getEndTime().equals("")) {
			// employee =
			// getHibernateTemplate().find("from HrEmployee hr where hr.hrDepartment.hrDepId='"+qp.getDepId()+"' and hr.hrMajor.hrMajId='"+qp.getMajId()+"' and hr.hrEmpRegistTime between to_date('"+qp.getStartTime()+"','yyyy-mm-dd') and to_date('"+qp.getEndTime()+"','yyyy-mm-dd'");
			employee = getHibernateTemplate().find(
					"from HrEmployee hr where hr.hrEmpRegistTime between to_date('"
							+ qp.getStartTime()
							+ "','yyyy-mm-dd') and to_date('" + qp.getEndTime()
							+ "','yyyy-mm-dd') and hr.hrDepartment.hrDepId='"
							+ qp.getDepId() + "' and hr.hrMajor.hrMajId='"
							+ qp.getMajId() + "'");
			return employee;
		}
		return null;

	}

	// 实现待审核信息
	public List<HrRemove> stayAuditing() {
		return (List<HrRemove>) getHibernateTemplate().find(
				"from HrRemove hr where hr.hrRemStatus='待审核'");
	}

	// 实现根据id查询方法
	public HrRemove remove(Integer id) {
		return getHibernateTemplate().get(HrRemove.class, id);
	}

	// 实现新薪酬查询
	public List<HrSalary> selectSalary() {
		return (List<HrSalary>) getHibernateTemplate().find("from HrSalary");
	}

	// 实现薪水id查询
	public HrSalary getByIdSalary(Integer id) {
		return getHibernateTemplate().get(HrSalary.class, id);
	}

	// 实现根据调动查询返回成功页面
	public List<HrRemove> sMove(QueryProperties qp) {
		List<HrRemove> employee = null;
		// 如果部门，职位，起始时间，结束时间为空进行全查

		if (qp.getDepId().equals("") && qp.getMajId().equals("")
				&& qp.getStartTime().equals("") && qp.getEndTime().equals("")) {
			employee = getHibernateTemplate().find("from HrRemove hr where hr.hrRemStatus='审核通过'");
			return employee;
		}
		// 如果部门，职位，起始时间，结束时间不为空进行查询
		else if (!qp.getDepId().equals("") && !qp.getMajId().equals("")
				&& !qp.getStartTime().equals("") && !qp.getEndTime().equals("")) {
			employee = getHibernateTemplate().find(
					"from HrRemove hr where hr.hrRemRegisterTime between to_date('"
							+ qp.getStartTime()
							+ "','yyyy-mm-dd') and to_date('" + qp.getEndTime()
							+ "','yyyy-mm-dd') and hr.hrDepartment.hrDepId='"
							+ qp.getDepId() + "' and hr.hrMajor.hrMajId='"
							+ qp.getMajId() + "' and hr.hrRemStatus='审核通过'");
			return employee;
		}
		return null;
	}
}