package com.rs.move.dao;

public class Dto {

	private Double hrSalRental;

	public Dto() {
		// TODO Auto-generated constructor stub
	}

	public Double getHrSalRental() {
		return hrSalRental;
	}
	public void setHrSalRental(Double hrSalRental) {
		this.hrSalRental = hrSalRental;
	}

	public Dto(Double hrSalRental) {
		super();
		this.hrSalRental = hrSalRental;
	}
	
}
