package com.rs.move.dao;

import java.sql.Date;

public class QueryProperties {

	private Integer depId;
	private Integer majId;
	private Date startTime;
	private Date endTime;
	
	public QueryProperties() {
	}

	public Integer getDepId() {
		return depId;
	}

	public void setDepId(Integer depId) {
		this.depId = depId;
	}

	public Integer getMajId() {
		return majId;
	}

	public void setMajId(Integer majId) {
		this.majId = majId;
	}


	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
}
