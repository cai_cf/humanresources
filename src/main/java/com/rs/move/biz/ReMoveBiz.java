package com.rs.move.biz;

import java.util.List;

import com.rs.common.entity.HrDepartment;
import com.rs.common.entity.HrEmployee;
import com.rs.common.entity.HrMajor;
import com.rs.common.entity.HrRemove;
import com.rs.common.entity.HrSalary;
import com.rs.move.dao.QueryProperties;


public interface ReMoveBiz {

	// 定义调动查询方法
	public List<HrRemove> removeList();
	
	// 定义根据ID查询单个对象
	public HrEmployee move(Integer id);
	
	// 定义查询新职位方法
	public List<HrMajor> selectMajor(Integer id);
	
	// 定义查询新部门方法
	public List<HrDepartment> selectDepartment();
	
	// 定义修改职位方法
	public void updateRemove(HrRemove move);
	
	// 定义添加职位方法
	public void addRemove(HrRemove move);
	
	// 定义模糊查询
	public List<HrEmployee> blurQuery(QueryProperties qp);
	
	// 查询待审核信息
	public List<HrRemove> stayAuditing();
	
	// 根据id进行查询
	public HrRemove remove(Integer id);
	
	// 薪酬标准
	public List<HrSalary> selectSalary();
	
	// 定义薪水查询，通过id
	public HrSalary getByIdSalary(Integer id);
	
	// 定义查询档案信息
	public HrEmployee employeeGetId(Integer id);
	
	// 定义修改档案信息
	public void updateEmployee(HrEmployee employee);
	
	// 定义根据调动查询信息，查询调动记录
	public List<HrRemove> sMove(QueryProperties qp);
}
