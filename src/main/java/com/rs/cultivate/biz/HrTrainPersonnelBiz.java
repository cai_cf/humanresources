package com.rs.cultivate.biz;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrTrainPersonnel;

public interface HrTrainPersonnelBiz {
	/**
	 * 作者:王运发
	 * 时间:2013年11月11日 16:10:12
	 */
  //添加培训记录
	public void addHrTrainPersonnel(HrTrainPersonnel hrp);
  //删除培训记录
	public void deleteHrTrainPersonnel(Integer id);
  //单查培训记录
	public  HrTrainPersonnel getHrTrainPersonnelById(Integer id);
  //修改培训记录
	public void upadteHrTrainPersonnel(HrTrainPersonnel hrp);
  //分页查所有的培训记录
   public  List<HrTrainPersonnel> getList(FenYe fenYe);

}
