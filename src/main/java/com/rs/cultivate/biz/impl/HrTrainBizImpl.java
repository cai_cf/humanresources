package com.rs.cultivate.biz.impl;

import java.util.List;

import com.rs.common.entity.FenYe;
import com.rs.common.entity.HrTrain;
import com.rs.cultivate.biz.HrTrainBiz;
import com.rs.cultivate.dao.HrTrainDao;

public class HrTrainBizImpl implements HrTrainBiz {

	private HrTrainDao hrTrainDao;
	
	public void setHrTrainDao(HrTrainDao hrTrainDao) {
		this.hrTrainDao = hrTrainDao;
	}
	//增加培训项目
	public void addHrTraion(HrTrain tr) {
		// TODO Auto-generated method stub
		hrTrainDao.addEntity(tr);
	}
  //删除培训项目
	public void deleteHrTrain(Integer id) {
		// TODO Auto-generated method stub
		hrTrainDao.deleteEntityById(HrTrain.class, id);
	}
   //全查培训项目
	public List<HrTrain> getAll() {
		// TODO Auto-generated method stub
		return hrTrainDao.getAll();
	}
  //单查培训项目
	public HrTrain getHrTrainById(Integer id) {
		// TODO Auto-generated method stub
		return (HrTrain) hrTrainDao.getEntityById(HrTrain.class, id);
	}
  //分页查询
	public List<HrTrain> getList(FenYe fenYe) {
		// TODO Auto-generated method stub
		return hrTrainDao.getList(HrTrain.class, fenYe);
	}
 //修改培训项目
	public void updateHrTraion(HrTrain tr) {
		// TODO Auto-generated method stub
		hrTrainDao.updateEntity(tr);
		
	}

}
