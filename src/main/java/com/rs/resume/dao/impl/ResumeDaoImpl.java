package com.rs.resume.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.rs.common.entity.HrResume;
import com.rs.resume.dao.QueryProperties;
import com.rs.resume.dao.ResumeDao;

public class ResumeDaoImpl extends HibernateDaoSupport implements ResumeDao {

	//进行添加方法实现
	public void addResume(HrResume resume) {
		getHibernateTemplate().save(resume);
	}
	
	//进行模糊查询发法
	public List<HrResume> getVague(QueryProperties qp) {
		//根据条件进行查询如果职业不为空,专业,工作经验不为空进行模糊查询
		if(!qp.getHrApplyMajor().equals("") && !qp.getHrPerEducated().equals("") && !qp.getHrPerJobRemark().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hs where  hs.hrPerEducated like '%"+qp.getHrPerEducated()+"%' and hs.hrApplyMajor like '%"+qp.getHrApplyMajor()+"%' and hs.hrPerJobRemark like '%"+qp.getHrPerJobRemark()+"%' and hr.hrPerStatus='待审核'");
			return resume;
		}
		// 如果职位和工作经验不为空的话进行模糊查询
		else if(!qp.getHrApplyMajor().equals("") && !qp.getHrPerJobRemark().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hs where hs.hrApplyMajor like '%"+qp.getHrApplyMajor()+"%' and hs.hrPerJobRemark like '%"+qp.getHrPerJobRemark()+"%' and hr.hrPerStatus='待审核'");
			return resume;
		}
		// 如果专业和工作经验不为空的话进行模糊查询
		else if(!qp.getHrPerEducated().equals("") && !qp.getHrPerJobRemark().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hs where  hs.hrPerEducated like '%"+qp.getHrPerEducated()+"%'  and hs.hrPerJobRemark like '%"+qp.getHrPerJobRemark()+"%' and hr.hrPerStatus='待审核'");
			return resume;
		}
		// 如果专业和职位不为空的话进行模糊查询
		else if(!qp.getHrApplyMajor().equals("") && !qp.getHrPerEducated().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hs where  hs.hrPerEducated like '%"+qp.getHrPerEducated()+"%' and hs.hrApplyMajor like '%"+qp.getHrApplyMajor()+"%' and hr.hrPerStatus='待审核'");
			return resume;
		}
		
		//如果查询职业不为空进行模糊查询
		else if(!qp.getHrApplyMajor().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hs where  hs.hrApplyMajor like '"+qp.getHrApplyMajor()+"' and hr.hrPerStatus='待审核'");
			return resume;
		}
		//如果专业不为空进行专业模糊查询
		else if(!qp.getHrPerEducated().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hs where  hs.hrPerEducated like '%"+qp.getHrPerEducated()+"%' and hr.hrPerStatus='待审核'");
			return resume;
		}
		//如果工作经验不为空的话进行模糊查询
		else if(!qp.getHrPerJobRemark().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hs where  hs.hrPerJobRemark like '%"+qp.getHrPerJobRemark()+"%' and hr.hrPerStatus='待审核'");
			return resume;
		}
		// 查询条件都为空的话，进行全查
		if(qp.getHrApplyMajor().equals("") && qp.getHrPerEducated().equals("") && qp.getHrPerJobRemark().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hr where hr.hrPerStatus='待审核'");
			return resume;
		}
		return null;
	}
		// 实现查询条件方法
	public List<HrResume> selectAll() {
		return (List<HrResume>)getHibernateTemplate().find("from HrResume");
	}
	// 实现根据id查询方法
	public HrResume hrResumeGetById(Integer id) {
		return getHibernateTemplate().get(HrResume.class, id);
	}
	// 实现修改方法实现
	public void updateResume(HrResume resume) {
		getHibernateTemplate().update(resume);
	}
	// 实现简历查询方法
	public List<HrResume> validResume(QueryProperties qp) {
		//根据条件进行查询如果职业不为空,专业,工作经验不为空进行模糊查询
		if(!qp.getHrApplyMajor().equals("") && !qp.getHrPerEducated().equals("") && !qp.getHrPerJobRemark().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hs where  hs.hrPerEducated like '%"+qp.getHrPerEducated()+"%' and hs.hrApplyMajor like '%"+qp.getHrApplyMajor()+"%' and hs.hrPerJobRemark like '%"+qp.getHrPerJobRemark()+"%' and hr.hrPerStatus='审核通过'");
			return resume;
		}
		// 如果职位和工作经验不为空的话进行模糊查询
		else if(!qp.getHrApplyMajor().equals("") && !qp.getHrPerJobRemark().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hs where hs.hrApplyMajor like '%"+qp.getHrApplyMajor()+"%' and hs.hrPerJobRemark like '%"+qp.getHrPerJobRemark()+"%' and hr.hrPerStatus='审核通过'");
			return resume;
		}
		// 如果专业和工作经验不为空的话进行模糊查询
		else if(!qp.getHrPerEducated().equals("") && !qp.getHrPerJobRemark().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hs where  hs.hrPerEducated like '%"+qp.getHrPerEducated()+"%'  and hs.hrPerJobRemark like '%"+qp.getHrPerJobRemark()+"%' and hr.hrPerStatus='审核通过'");
			return resume;
		}
		// 如果专业和职位不为空的话进行模糊查询
		else if(!qp.getHrApplyMajor().equals("") && !qp.getHrPerEducated().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hs where  hs.hrPerEducated like '%"+qp.getHrPerEducated()+"%' and hs.hrApplyMajor like '%"+qp.getHrApplyMajor()+"%' and hr.hrPerStatus='审核通过'");
			return resume;
		}
		//如果查询职业不为空进行模糊查询
		else if(!qp.getHrApplyMajor().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hs where  hs.hrApplyMajor like '"+qp.getHrApplyMajor()+"' and hr.hrPerStatus='审核通过'");
			return resume;
		}
		//如果专业不为空进行专业模糊查询
		else if(!qp.getHrPerEducated().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hs where  hs.hrPerEducated like '%"+qp.getHrPerEducated()+"%' and hr.hrPerStatus='审核通过'");
			return resume;
		}
		//如果工作经验不为空的话进行模糊查询
		else if(!qp.getHrPerJobRemark().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hs where  hs.hrPerJobRemark like '%"+qp.getHrPerJobRemark()+"%' and hr.hrPerStatus='审核通过'");
			return resume;
		}
		// 查询条件都为空的话，进行全查
		if(qp.getHrApplyMajor().equals("") && qp.getHrPerEducated().equals("") && qp.getHrPerJobRemark().equals("")){
			List<HrResume> resume=getHibernateTemplate().find("from HrResume hr where hr.hrPerStatus='审核通过'");
			return resume;
		}
		return null;
	}
}
