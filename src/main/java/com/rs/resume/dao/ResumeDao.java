package com.rs.resume.dao;

import java.util.List;

import com.rs.common.entity.HrResume;

public interface ResumeDao {

	//定义添加简历方法
	public void addResume(HrResume resume);
	
	//进行模糊查询发法定义
	public List<HrResume> getVague(QueryProperties qp);
	
	// 定义全查方法
	public List<HrResume> selectAll();
	
	// 定义id查询方法
	public HrResume hrResumeGetById(Integer id);
	
	// 定义修改方法
	public void updateResume(HrResume resume);
	
	// 定义简历查询
	public List<HrResume> validResume(QueryProperties qp);
}
