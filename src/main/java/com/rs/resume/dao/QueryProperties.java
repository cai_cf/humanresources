package com.rs.resume.dao;

public class QueryProperties {

	//本类说明减少模糊查询属性字段封装
	 private String hrPerEducated; //专业
	 private String hrApplyMajor; //职位
	 private String hrPerJobRemark; //工作经验
	
	 
	 /**
	  * 此处为封住get set方法,目的增加程序安全性
	  */
	 
	 public String getHrPerEducated() {
			return hrPerEducated;
		}
		public void setHrPerEducated(String hrPerEducated) {
			this.hrPerEducated = hrPerEducated;
		}
		public String getHrApplyMajor() {
			return hrApplyMajor;
		}
		public void setHrApplyMajor(String hrApplyMajor) {
			this.hrApplyMajor = hrApplyMajor;
		}
		public String getHrPerJobRemark() {
			return hrPerJobRemark;
		}
		public void setHrPerJobRemark(String hrPerJobRemark) {
			this.hrPerJobRemark = hrPerJobRemark;
		}
}
