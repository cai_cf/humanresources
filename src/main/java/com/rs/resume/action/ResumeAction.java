package com.rs.resume.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.rs.common.entity.HrResume;
import com.rs.resume.biz.ResumeBiz;
import com.rs.resume.dao.QueryProperties;

/**
 * 简历管理模块
 * @author 贾志新
 */

public class ResumeAction extends ActionSupport implements RequestAware {

	// 进行biz层注入
	private ResumeBiz resumeBiz;
	// 进行对象添加
	private HrResume resume;
	// 调用属性封住类
	private QueryProperties queryProperties;
	// 根据id进行查询
	private Integer id;
	
	
	// 进行set 封装
	public void setId(Integer id) {
		this.id = id;
	}
	//进行get 封装
	public QueryProperties getQueryProperties() {
		return queryProperties;
	}
	//进行set封装
	public void setQueryProperties(QueryProperties queryProperties) {
		this.queryProperties = queryProperties;
	}
	// 进行biz层封装
	public void setResumeBiz(ResumeBiz resumeBiz) {
		this.resumeBiz = resumeBiz;
	}
	// 对象get封装
	public HrResume getResume() {
		return resume;
	}
	//让页面注入
	public void setResume(HrResume resume) {
		this.resume = resume;
	}

	// 进行公用配置
	Map<String,Object> request;
	public void setRequest(Map<String, Object> request) {
			this.request=request;
	}

	// 进行简历添加方法实现
	public String addResume() {
		try {
			/**
			 * 写try catch 目的
			 * 如果出错返回出错页面
			 * 如果成功进入成功界面
			 */
			resumeBiz.addResume(resume);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	
	//进行模糊查询
	public String vagueResume(){
		try {
			List<HrResume> resume=resumeBiz.getVague(queryProperties);
			//将查询出的信息放到集合中
			request.put("resume", resume);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	
	// 实现全查
	public String selectResume(){
		try {
			/**
			 * 此处写 try catch目的
			 * 如果成功的话，返回查询页面
			 * 如果失败的话，返回失败页面
			 */
			// 实现全查集合
			List<HrResume> selectAll = resumeBiz.selectAll();
			// 将查询信息放入集合
			request.put("selectAll", selectAll);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 根据id进行查询
	public String compoundResume(){
		try {
			/**
			 * 此处的try catch目的
			 * 如果成功进行成功显示
			 * 如果失败进行失败显示
			 */
			// 根据id进行查询
			HrResume hrResume = resumeBiz.hrResumeGetById(id);
			request.put("hrResume", hrResume);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 简历修改
	public String updateResume(){
		try {
			/**
			 * 此处写try catch目的
			 * 如果成功的话跳转到成功页面
			 * 如果失败的话跳转到失败页面
			 */
			resumeBiz.updateResume(resume);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
	// 有效简历查询
	public String validResume(){
		try {
			List<HrResume> resume=resumeBiz.validResume(queryProperties);
			request.put("resume", resume);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ERROR;
	}
}
