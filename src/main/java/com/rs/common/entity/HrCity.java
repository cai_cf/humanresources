package com.rs.common.entity;

/**
 * HrCity entity. @author MyEclipse Persistence Tools
 */

public class HrCity implements java.io.Serializable {

	// Fields

	private Integer hrCityId;
	private HrProvince hrProvince;
	private String hrCityName;

	// Constructors

	/** default constructor */
	public HrCity() {
	}

	/** full constructor */
	public HrCity(HrProvince hrProvince, String hrCityName) {
		this.hrProvince = hrProvince;
		this.hrCityName = hrCityName;
	}

	// Property accessors

	public Integer getHrCityId() {
		return this.hrCityId;
	}

	public void setHrCityId(Integer hrCityId) {
		this.hrCityId = hrCityId;
	}

	public HrProvince getHrProvince() {
		return this.hrProvince;
	}

	public void setHrProvince(HrProvince hrProvince) {
		this.hrProvince = hrProvince;
	}

	public String getHrCityName() {
		return this.hrCityName;
	}

	public void setHrCityName(String hrCityName) {
		this.hrCityName = hrCityName;
	}

}