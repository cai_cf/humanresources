package com.rs.common.entity;

/**
 * HrUser entity. @author MyEclipse Persistence Tools
 */

public class HrUser implements java.io.Serializable {

	// Fields

	private Integer hrUserId;
	private Integer hrEmpId;
	private String hrUserPwd;
	private String hrUserName;
	private String hrUserDegree;

	// Constructors

	/** default constructor */
	public HrUser() {
	}

	/** full constructor */
	public HrUser(Integer hrEmpId, String hrUserPwd, String hrUserName,
			String hrUserDegree) {
		this.hrEmpId = hrEmpId;
		this.hrUserPwd = hrUserPwd;
		this.hrUserName = hrUserName;
		this.hrUserDegree = hrUserDegree;
	}

	// Property accessors

	public Integer getHrUserId() {
		return this.hrUserId;
	}

	public void setHrUserId(Integer hrUserId) {
		this.hrUserId = hrUserId;
	}

	public Integer getHrEmpId() {
		return this.hrEmpId;
	}

	public void setHrEmpId(Integer hrEmpId) {
		this.hrEmpId = hrEmpId;
	}

	public String getHrUserPwd() {
		return this.hrUserPwd;
	}

	public void setHrUserPwd(String hrUserPwd) {
		this.hrUserPwd = hrUserPwd;
	}

	public String getHrUserName() {
		return this.hrUserName;
	}

	public void setHrUserName(String hrUserName) {
		this.hrUserName = hrUserName;
	}

	public String getHrUserDegree() {
		return this.hrUserDegree;
	}

	public void setHrUserDegree(String hrUserDegree) {
		this.hrUserDegree = hrUserDegree;
	}

}