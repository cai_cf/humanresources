package com.rs.common.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * HrDepartment entity. @author MyEclipse Persistence Tools
 */

public class HrDepartment implements java.io.Serializable {

	// Fields

	private Integer hrDepId;
	private String hrDepName;
	private Set hrRemoves = new HashSet(0);
	private Set hrMajors = new HashSet(0);
	private Set hrEmployees = new HashSet(0);

	// Constructors

	/** default constructor */
	public HrDepartment() {
	}

	/** full constructor */
	public HrDepartment(String hrDepName, Set hrRemoves, Set hrMajors,
			Set hrEmployees) {
		this.hrDepName = hrDepName;
		this.hrRemoves = hrRemoves;
		this.hrMajors = hrMajors;
		this.hrEmployees = hrEmployees;
	}

	// Property accessors

	public Integer getHrDepId() {
		return this.hrDepId;
	}

	public void setHrDepId(Integer hrDepId) {
		this.hrDepId = hrDepId;
	}

	public String getHrDepName() {
		return this.hrDepName;
	}

	public void setHrDepName(String hrDepName) {
		this.hrDepName = hrDepName;
	}

	public Set getHrRemoves() {
		return this.hrRemoves;
	}

	public void setHrRemoves(Set hrRemoves) {
		this.hrRemoves = hrRemoves;
	}

	public Set getHrMajors() {
		return this.hrMajors;
	}

	public void setHrMajors(Set hrMajors) {
		this.hrMajors = hrMajors;
	}

	public Set getHrEmployees() {
		return this.hrEmployees;
	}

	public void setHrEmployees(Set hrEmployees) {
		this.hrEmployees = hrEmployees;
	}

}