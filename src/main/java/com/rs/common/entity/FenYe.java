package com.rs.common.entity;

import java.util.List;

public class FenYe {
	private int nowPage;// 当前页
	private int pageSize;// 每页条数
	private int totalNums;// 总记录数
	private int totalPages;// 总页数

	public FenYe() {
		// TODO Auto-generated constructor stub
	}

	public FenYe(int nowPage, int pageSize, int totalNums, int totalPages
			) {
		super();
		this.nowPage = nowPage;
		this.pageSize = pageSize;
		this.totalNums = totalNums;
		this.totalPages = totalPages;
	
	}

	public int getNowPage() {
		return nowPage;
	}

	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalNums() {
		return totalNums;
	}

	public void setTotalNums(int totalNums) {
		this.totalNums = totalNums;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}



}
