package com.rs.common.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * HrEmployee entity. @author MyEclipse Persistence Tools
 */

public class HrEmployee implements java.io.Serializable {

	// Fields

	private Integer hrEmpId;
	private HrSalary hrSalary;
	private HrMajor hrMajor;
	private HrDepartment hrDepartment;
	private String hrEmpName;
	private String hrEmpSex;
	private Integer hrEmpAge;
	private String hrEmpEmail;
	private String hrEmpQq;
	private Date hrEmpBirthday;
	private String hrEmpDegree;
	private String hrEmpEducated;
	private String hrEmpCard;
	private String hrEmpNative;
	private String hrEmpFolk;
	private String hrEmpGovernment;
	private String hrEmpReligion;
	private String hrEmpMarriage;
	private Date hrEmpJobDater;
	private String hrEmpPhone;
	private String hrEmpAddress;
	private String hrEmpPostcode;
	private String hrEmpOpenBank;
	private String hrEmpSpeciality;
	private String hrEmpHobby;
	private String hrEmpRegisterMan;
	private String hrEmpResume;
	private String hrEmpRemarks;
	private Date hrEmpRegistTime;
	private Integer hrEmpCheckStatus;
	private Integer hrEmpFileStatus;
	private Integer hrEmpJobStatus;
	private String hrEmpPhoto;
	private Set hrTrainPersonnels = new HashSet(0);
	private Set hrRemoves = new HashSet(0);
	private Set hrRewards = new HashSet(0);

	// Constructors

	/** default constructor */
	public HrEmployee() {
	}

	/** full constructor */
	public HrEmployee(HrSalary hrSalary, HrMajor hrMajor,
			HrDepartment hrDepartment, String hrEmpName, String hrEmpSex,
			Integer hrEmpAge, String hrEmpEmail, String hrEmpQq,
			Date hrEmpBirthday, String hrEmpDegree, String hrEmpEducated,
			String hrEmpCard, String hrEmpNative, String hrEmpFolk,
			String hrEmpGovernment, String hrEmpReligion, String hrEmpMarriage,
			Date hrEmpJobDater, String hrEmpPhone, String hrEmpAddress,
			String hrEmpPostcode, String hrEmpOpenBank, String hrEmpSpeciality,
			String hrEmpHobby, String hrEmpRegisterMan, String hrEmpResume,
			String hrEmpRemarks, Date hrEmpRegistTime,
			Integer hrEmpCheckStatus, Integer hrEmpFileStatus,
			Integer hrEmpJobStatus, String hrEmpPhoto, Set hrTrainPersonnels,
			Set hrRemoves, Set hrRewards) {
		this.hrSalary = hrSalary;
		this.hrMajor = hrMajor;
		this.hrDepartment = hrDepartment;
		this.hrEmpName = hrEmpName;
		this.hrEmpSex = hrEmpSex;
		this.hrEmpAge = hrEmpAge;
		this.hrEmpEmail = hrEmpEmail;
		this.hrEmpQq = hrEmpQq;
		this.hrEmpBirthday = hrEmpBirthday;
		this.hrEmpDegree = hrEmpDegree;
		this.hrEmpEducated = hrEmpEducated;
		this.hrEmpCard = hrEmpCard;
		this.hrEmpNative = hrEmpNative;
		this.hrEmpFolk = hrEmpFolk;
		this.hrEmpGovernment = hrEmpGovernment;
		this.hrEmpReligion = hrEmpReligion;
		this.hrEmpMarriage = hrEmpMarriage;
		this.hrEmpJobDater = hrEmpJobDater;
		this.hrEmpPhone = hrEmpPhone;
		this.hrEmpAddress = hrEmpAddress;
		this.hrEmpPostcode = hrEmpPostcode;
		this.hrEmpOpenBank = hrEmpOpenBank;
		this.hrEmpSpeciality = hrEmpSpeciality;
		this.hrEmpHobby = hrEmpHobby;
		this.hrEmpRegisterMan = hrEmpRegisterMan;
		this.hrEmpResume = hrEmpResume;
		this.hrEmpRemarks = hrEmpRemarks;
		this.hrEmpRegistTime = hrEmpRegistTime;
		this.hrEmpCheckStatus = hrEmpCheckStatus;
		this.hrEmpFileStatus = hrEmpFileStatus;
		this.hrEmpJobStatus = hrEmpJobStatus;
		this.hrEmpPhoto = hrEmpPhoto;
		this.hrTrainPersonnels = hrTrainPersonnels;
		this.hrRemoves = hrRemoves;
		this.hrRewards = hrRewards;
	}

	// Property accessors

	public Integer getHrEmpId() {
		return this.hrEmpId;
	}

	public void setHrEmpId(Integer hrEmpId) {
		this.hrEmpId = hrEmpId;
	}

	public HrSalary getHrSalary() {
		return this.hrSalary;
	}

	public void setHrSalary(HrSalary hrSalary) {
		this.hrSalary = hrSalary;
	}

	public HrMajor getHrMajor() {
		return this.hrMajor;
	}

	public void setHrMajor(HrMajor hrMajor) {
		this.hrMajor = hrMajor;
	}

	public HrDepartment getHrDepartment() {
		return this.hrDepartment;
	}

	public void setHrDepartment(HrDepartment hrDepartment) {
		this.hrDepartment = hrDepartment;
	}

	public String getHrEmpName() {
		return this.hrEmpName;
	}

	public void setHrEmpName(String hrEmpName) {
		this.hrEmpName = hrEmpName;
	}

	public String getHrEmpSex() {
		return this.hrEmpSex;
	}

	public void setHrEmpSex(String hrEmpSex) {
		this.hrEmpSex = hrEmpSex;
	}

	public Integer getHrEmpAge() {
		return this.hrEmpAge;
	}

	public void setHrEmpAge(Integer hrEmpAge) {
		this.hrEmpAge = hrEmpAge;
	}

	public String getHrEmpEmail() {
		return this.hrEmpEmail;
	}

	public void setHrEmpEmail(String hrEmpEmail) {
		this.hrEmpEmail = hrEmpEmail;
	}

	public String getHrEmpQq() {
		return this.hrEmpQq;
	}

	public void setHrEmpQq(String hrEmpQq) {
		this.hrEmpQq = hrEmpQq;
	}

	public Date getHrEmpBirthday() {
		return this.hrEmpBirthday;
	}

	public void setHrEmpBirthday(Date hrEmpBirthday) {
		this.hrEmpBirthday = hrEmpBirthday;
	}

	public String getHrEmpDegree() {
		return this.hrEmpDegree;
	}

	public void setHrEmpDegree(String hrEmpDegree) {
		this.hrEmpDegree = hrEmpDegree;
	}

	public String getHrEmpEducated() {
		return this.hrEmpEducated;
	}

	public void setHrEmpEducated(String hrEmpEducated) {
		this.hrEmpEducated = hrEmpEducated;
	}

	public String getHrEmpCard() {
		return this.hrEmpCard;
	}

	public void setHrEmpCard(String hrEmpCard) {
		this.hrEmpCard = hrEmpCard;
	}

	public String getHrEmpNative() {
		return this.hrEmpNative;
	}

	public void setHrEmpNative(String hrEmpNative) {
		this.hrEmpNative = hrEmpNative;
	}

	public String getHrEmpFolk() {
		return this.hrEmpFolk;
	}

	public void setHrEmpFolk(String hrEmpFolk) {
		this.hrEmpFolk = hrEmpFolk;
	}

	public String getHrEmpGovernment() {
		return this.hrEmpGovernment;
	}

	public void setHrEmpGovernment(String hrEmpGovernment) {
		this.hrEmpGovernment = hrEmpGovernment;
	}

	public String getHrEmpReligion() {
		return this.hrEmpReligion;
	}

	public void setHrEmpReligion(String hrEmpReligion) {
		this.hrEmpReligion = hrEmpReligion;
	}

	public String getHrEmpMarriage() {
		return this.hrEmpMarriage;
	}

	public void setHrEmpMarriage(String hrEmpMarriage) {
		this.hrEmpMarriage = hrEmpMarriage;
	}

	public Date getHrEmpJobDater() {
		return this.hrEmpJobDater;
	}

	public void setHrEmpJobDater(Date hrEmpJobDater) {
		this.hrEmpJobDater = hrEmpJobDater;
	}

	public String getHrEmpPhone() {
		return this.hrEmpPhone;
	}

	public void setHrEmpPhone(String hrEmpPhone) {
		this.hrEmpPhone = hrEmpPhone;
	}

	public String getHrEmpAddress() {
		return this.hrEmpAddress;
	}

	public void setHrEmpAddress(String hrEmpAddress) {
		this.hrEmpAddress = hrEmpAddress;
	}

	public String getHrEmpPostcode() {
		return this.hrEmpPostcode;
	}

	public void setHrEmpPostcode(String hrEmpPostcode) {
		this.hrEmpPostcode = hrEmpPostcode;
	}

	public String getHrEmpOpenBank() {
		return this.hrEmpOpenBank;
	}

	public void setHrEmpOpenBank(String hrEmpOpenBank) {
		this.hrEmpOpenBank = hrEmpOpenBank;
	}

	public String getHrEmpSpeciality() {
		return this.hrEmpSpeciality;
	}

	public void setHrEmpSpeciality(String hrEmpSpeciality) {
		this.hrEmpSpeciality = hrEmpSpeciality;
	}

	public String getHrEmpHobby() {
		return this.hrEmpHobby;
	}

	public void setHrEmpHobby(String hrEmpHobby) {
		this.hrEmpHobby = hrEmpHobby;
	}

	public String getHrEmpRegisterMan() {
		return this.hrEmpRegisterMan;
	}

	public void setHrEmpRegisterMan(String hrEmpRegisterMan) {
		this.hrEmpRegisterMan = hrEmpRegisterMan;
	}

	public String getHrEmpResume() {
		return this.hrEmpResume;
	}

	public void setHrEmpResume(String hrEmpResume) {
		this.hrEmpResume = hrEmpResume;
	}

	public String getHrEmpRemarks() {
		return this.hrEmpRemarks;
	}

	public void setHrEmpRemarks(String hrEmpRemarks) {
		this.hrEmpRemarks = hrEmpRemarks;
	}

	public Date getHrEmpRegistTime() {
		return this.hrEmpRegistTime;
	}

	public void setHrEmpRegistTime(Date hrEmpRegistTime) {
		this.hrEmpRegistTime = hrEmpRegistTime;
	}

	public Integer getHrEmpCheckStatus() {
		return this.hrEmpCheckStatus;
	}

	public void setHrEmpCheckStatus(Integer hrEmpCheckStatus) {
		this.hrEmpCheckStatus = hrEmpCheckStatus;
	}

	public Integer getHrEmpFileStatus() {
		return this.hrEmpFileStatus;
	}

	public void setHrEmpFileStatus(Integer hrEmpFileStatus) {
		this.hrEmpFileStatus = hrEmpFileStatus;
	}

	public Integer getHrEmpJobStatus() {
		return this.hrEmpJobStatus;
	}

	public void setHrEmpJobStatus(Integer hrEmpJobStatus) {
		this.hrEmpJobStatus = hrEmpJobStatus;
	}

	public String getHrEmpPhoto() {
		return this.hrEmpPhoto;
	}

	public void setHrEmpPhoto(String hrEmpPhoto) {
		this.hrEmpPhoto = hrEmpPhoto;
	}

	public Set getHrTrainPersonnels() {
		return this.hrTrainPersonnels;
	}

	public void setHrTrainPersonnels(Set hrTrainPersonnels) {
		this.hrTrainPersonnels = hrTrainPersonnels;
	}

	public Set getHrRemoves() {
		return this.hrRemoves;
	}

	public void setHrRemoves(Set hrRemoves) {
		this.hrRemoves = hrRemoves;
	}

	public Set getHrRewards() {
		return this.hrRewards;
	}

	public void setHrRewards(Set hrRewards) {
		this.hrRewards = hrRewards;
	}

}