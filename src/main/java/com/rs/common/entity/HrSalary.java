package com.rs.common.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * HrSalary entity. @author MyEclipse Persistence Tools
 */

public class HrSalary implements java.io.Serializable {

	// Fields

	private Integer hrSalId;
	private HrMajor hrMajor;
	private String hrSalName;
	private Double hrSalRental;
	private String hrSalRegister;
	private Date hrSalTime;
	private String hrSalRemark;
	private Double hrSalBase;
	private Double hrSalEvection;
	private Double hrSalTraffic;
	private Double hrSalMessage;
	private Double hrSalFood;
	private Set hrEmployees = new HashSet(0);

	// Constructors

	/** default constructor */
	public HrSalary() {
	}

	/** full constructor */
	public HrSalary(HrMajor hrMajor, String hrSalName, Double hrSalRental,
			String hrSalRegister, Date hrSalTime, String hrSalRemark,
			Double hrSalBase, Double hrSalEvection, Double hrSalTraffic,
			Double hrSalMessage, Double hrSalFood, Set hrEmployees) {
		this.hrMajor = hrMajor;
		this.hrSalName = hrSalName;
		this.hrSalRental = hrSalRental;
		this.hrSalRegister = hrSalRegister;
		this.hrSalTime = hrSalTime;
		this.hrSalRemark = hrSalRemark;
		this.hrSalBase = hrSalBase;
		this.hrSalEvection = hrSalEvection;
		this.hrSalTraffic = hrSalTraffic;
		this.hrSalMessage = hrSalMessage;
		this.hrSalFood = hrSalFood;
		this.hrEmployees = hrEmployees;
	}

	// Property accessors

	public Integer getHrSalId() {
		return this.hrSalId;
	}

	public void setHrSalId(Integer hrSalId) {
		this.hrSalId = hrSalId;
	}

	public HrMajor getHrMajor() {
		return this.hrMajor;
	}

	public void setHrMajor(HrMajor hrMajor) {
		this.hrMajor = hrMajor;
	}

	public String getHrSalName() {
		return this.hrSalName;
	}

	public void setHrSalName(String hrSalName) {
		this.hrSalName = hrSalName;
	}

	public Double getHrSalRental() {
		return this.hrSalRental;
	}

	public void setHrSalRental(Double hrSalRental) {
		this.hrSalRental = hrSalRental;
	}

	public String getHrSalRegister() {
		return this.hrSalRegister;
	}

	public void setHrSalRegister(String hrSalRegister) {
		this.hrSalRegister = hrSalRegister;
	}

	public Date getHrSalTime() {
		return this.hrSalTime;
	}

	public void setHrSalTime(Date hrSalTime) {
		this.hrSalTime = hrSalTime;
	}

	public String getHrSalRemark() {
		return this.hrSalRemark;
	}

	public void setHrSalRemark(String hrSalRemark) {
		this.hrSalRemark = hrSalRemark;
	}

	public Double getHrSalBase() {
		return this.hrSalBase;
	}

	public void setHrSalBase(Double hrSalBase) {
		this.hrSalBase = hrSalBase;
	}

	public Double getHrSalEvection() {
		return this.hrSalEvection;
	}

	public void setHrSalEvection(Double hrSalEvection) {
		this.hrSalEvection = hrSalEvection;
	}

	public Double getHrSalTraffic() {
		return this.hrSalTraffic;
	}

	public void setHrSalTraffic(Double hrSalTraffic) {
		this.hrSalTraffic = hrSalTraffic;
	}

	public Double getHrSalMessage() {
		return this.hrSalMessage;
	}

	public void setHrSalMessage(Double hrSalMessage) {
		this.hrSalMessage = hrSalMessage;
	}

	public Double getHrSalFood() {
		return this.hrSalFood;
	}

	public void setHrSalFood(Double hrSalFood) {
		this.hrSalFood = hrSalFood;
	}

	public Set getHrEmployees() {
		return this.hrEmployees;
	}

	public void setHrEmployees(Set hrEmployees) {
		this.hrEmployees = hrEmployees;
	}

}