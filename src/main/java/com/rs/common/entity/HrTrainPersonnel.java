package com.rs.common.entity;

/**
 * HrTrainPersonnel entity. @author MyEclipse Persistence Tools
 */

public class HrTrainPersonnel implements java.io.Serializable {

	// Fields

	private Integer hrPerId;
	private HrTrain hrTrain;
	private HrEmployee hrEmployee;
	private String hrPerDegree;
	private String hrPerRemark;

	// Constructors

	/** default constructor */
	public HrTrainPersonnel() {
	}

	/** full constructor */
	public HrTrainPersonnel(HrTrain hrTrain, HrEmployee hrEmployee,
			String hrPerDegree, String hrPerRemark) {
		this.hrTrain = hrTrain;
		this.hrEmployee = hrEmployee;
		this.hrPerDegree = hrPerDegree;
		this.hrPerRemark = hrPerRemark;
	}

	// Property accessors

	public Integer getHrPerId() {
		return this.hrPerId;
	}

	public void setHrPerId(Integer hrPerId) {
		this.hrPerId = hrPerId;
	}

	public HrTrain getHrTrain() {
		return this.hrTrain;
	}

	public void setHrTrain(HrTrain hrTrain) {
		this.hrTrain = hrTrain;
	}

	public HrEmployee getHrEmployee() {
		return this.hrEmployee;
	}

	public void setHrEmployee(HrEmployee hrEmployee) {
		this.hrEmployee = hrEmployee;
	}

	public String getHrPerDegree() {
		return this.hrPerDegree;
	}

	public void setHrPerDegree(String hrPerDegree) {
		this.hrPerDegree = hrPerDegree;
	}

	public String getHrPerRemark() {
		return this.hrPerRemark;
	}

	public void setHrPerRemark(String hrPerRemark) {
		this.hrPerRemark = hrPerRemark;
	}

}