package com.rs.common.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * HrMajor entity. @author MyEclipse Persistence Tools
 */

public class HrMajor implements java.io.Serializable {

	// Fields

	private Integer hrMajId;
	private HrDepartment hrDepartment;
	private String hrMajName;
	private Set hrRemoves = new HashSet(0);
	private Set hrEmployees = new HashSet(0);
	private Set hrSalaries = new HashSet(0);

	// Constructors

	/** default constructor */
	public HrMajor() {
	}

	/** full constructor */
	public HrMajor(HrDepartment hrDepartment, String hrMajName, Set hrRemoves,
			Set hrEmployees, Set hrSalaries) {
		this.hrDepartment = hrDepartment;
		this.hrMajName = hrMajName;
		this.hrRemoves = hrRemoves;
		this.hrEmployees = hrEmployees;
		this.hrSalaries = hrSalaries;
	}

	// Property accessors

	public Integer getHrMajId() {
		return this.hrMajId;
	}

	public void setHrMajId(Integer hrMajId) {
		this.hrMajId = hrMajId;
	}

	public HrDepartment getHrDepartment() {
		return this.hrDepartment;
	}

	public void setHrDepartment(HrDepartment hrDepartment) {
		this.hrDepartment = hrDepartment;
	}

	public String getHrMajName() {
		return this.hrMajName;
	}

	public void setHrMajName(String hrMajName) {
		this.hrMajName = hrMajName;
	}

	public Set getHrRemoves() {
		return this.hrRemoves;
	}

	public void setHrRemoves(Set hrRemoves) {
		this.hrRemoves = hrRemoves;
	}

	public Set getHrEmployees() {
		return this.hrEmployees;
	}

	public void setHrEmployees(Set hrEmployees) {
		this.hrEmployees = hrEmployees;
	}

	public Set getHrSalaries() {
		return this.hrSalaries;
	}

	public void setHrSalaries(Set hrSalaries) {
		this.hrSalaries = hrSalaries;
	}

}