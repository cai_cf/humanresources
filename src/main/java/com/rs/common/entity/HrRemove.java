package com.rs.common.entity;

import java.util.Date;

/**
 * HrRemove entity. @author MyEclipse Persistence Tools
 */

public class HrRemove implements java.io.Serializable {

	// Fields

	private Integer hrRemId;
	private HrDepartment hrDepartment;
	private HrMajor hrMajor;
	private HrEmployee hrEmployee;
	private String hrDepOldName;
	private String hrMajOldName;
	private String hrRemCause;
	private Date hrRemTime;
	private String hrRemStatus;
	private String hrRemRegisterMan;
	private Date hrRemRegisterTime;

	// Constructors

	/** default constructor */
	public HrRemove() {
	}

	/** full constructor */
	public HrRemove(HrDepartment hrDepartment, HrMajor hrMajor,
			HrEmployee hrEmployee, String hrDepOldName, String hrMajOldName,
			String hrRemCause, Date hrRemTime, String hrRemStatus,
			String hrRemRegisterMan, Date hrRemRegisterTime) {
		this.hrDepartment = hrDepartment;
		this.hrMajor = hrMajor;
		this.hrEmployee = hrEmployee;
		this.hrDepOldName = hrDepOldName;
		this.hrMajOldName = hrMajOldName;
		this.hrRemCause = hrRemCause;
		this.hrRemTime = hrRemTime;
		this.hrRemStatus = hrRemStatus;
		this.hrRemRegisterMan = hrRemRegisterMan;
		this.hrRemRegisterTime = hrRemRegisterTime;
	}

	// Property accessors

	public Integer getHrRemId() {
		return this.hrRemId;
	}

	public void setHrRemId(Integer hrRemId) {
		this.hrRemId = hrRemId;
	}

	public HrDepartment getHrDepartment() {
		return this.hrDepartment;
	}

	public void setHrDepartment(HrDepartment hrDepartment) {
		this.hrDepartment = hrDepartment;
	}

	public HrMajor getHrMajor() {
		return this.hrMajor;
	}

	public void setHrMajor(HrMajor hrMajor) {
		this.hrMajor = hrMajor;
	}

	public HrEmployee getHrEmployee() {
		return this.hrEmployee;
	}

	public void setHrEmployee(HrEmployee hrEmployee) {
		this.hrEmployee = hrEmployee;
	}

	public String getHrDepOldName() {
		return this.hrDepOldName;
	}

	public void setHrDepOldName(String hrDepOldName) {
		this.hrDepOldName = hrDepOldName;
	}

	public String getHrMajOldName() {
		return this.hrMajOldName;
	}

	public void setHrMajOldName(String hrMajOldName) {
		this.hrMajOldName = hrMajOldName;
	}

	public String getHrRemCause() {
		return this.hrRemCause;
	}

	public void setHrRemCause(String hrRemCause) {
		this.hrRemCause = hrRemCause;
	}

	public Date getHrRemTime() {
		return this.hrRemTime;
	}

	public void setHrRemTime(Date hrRemTime) {
		this.hrRemTime = hrRemTime;
	}

	public String getHrRemStatus() {
		return this.hrRemStatus;
	}

	public void setHrRemStatus(String hrRemStatus) {
		this.hrRemStatus = hrRemStatus;
	}

	public String getHrRemRegisterMan() {
		return this.hrRemRegisterMan;
	}

	public void setHrRemRegisterMan(String hrRemRegisterMan) {
		this.hrRemRegisterMan = hrRemRegisterMan;
	}

	public Date getHrRemRegisterTime() {
		return this.hrRemRegisterTime;
	}

	public void setHrRemRegisterTime(Date hrRemRegisterTime) {
		this.hrRemRegisterTime = hrRemRegisterTime;
	}

}