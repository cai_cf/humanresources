package com.rs.common.entity;

import java.util.Date;

public class QueryProperties {

	private int depId;
	private int majId;
	private Date startTime;
	private Date endTime;
	public int getDepId() {
		return depId;
	}
	public void setDepId(int depId) {
		this.depId = depId;
	}
	public int getMajId() {
		return majId;
	}
	public void setMajId(int majId) {
		this.majId = majId;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public QueryProperties() {
		// TODO Auto-generated constructor stub
	}
	public QueryProperties(int depId, int majId, Date startTime, Date endTime) {
		super();
		this.depId = depId;
		this.majId = majId;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
}
