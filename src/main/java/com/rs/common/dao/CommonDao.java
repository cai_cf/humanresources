package com.rs.common.dao;

import java.io.Serializable;
import java.util.List;

import com.rs.common.entity.FenYe;

public interface CommonDao<T> {
	void addEntity(Object entity);

	void updateEntity(Object entity);

	void deleteEntityById(Class<T> entityClass, Serializable id);

	T getEntityById(Class<T> entityClass, Serializable id);

	List<T> getList(Class<T> entityClass, final int nowPage, final int pageSize);

	List<T> getList(Class<T> entityClass, final FenYe fenye);
}
